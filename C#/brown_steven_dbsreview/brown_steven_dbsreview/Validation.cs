﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace brown_steven_dbsreview
{
    class Validation
    {
        public static int GetInt(string message = "Enter an integer: ")
        {
            int validatedInt;
            string input = null;

            do
            {
                Console.Write(message);
                input = Console.ReadLine();
            } while (!Int32.TryParse(input, out validatedInt));

            return validatedInt;
        }

        public static int GetInt(int min, int max, string message = "Enter an integer:")
        {
            int validatedInt;
            string input = null;
            string errorMessage = "";
            bool validResponse = false;

            do
            {
                Console.Write(message);
                input = Console.ReadLine();
                if (Int32.TryParse(input, out validatedInt)) {
                    if (validatedInt >= min && validatedInt <= max)
                    {
                        validResponse = true;
                    }
                }
                if (!validResponse) { 
                    errorMessage = "\n\tERROR: You did not enter a valid number. Please try again.\n ";
                    Console.WriteLine(errorMessage);
                }
            } while (!validResponse);
            return validatedInt;
        }

        public static int GetYear(int minYear = 0, string message = "Enter a year: ")
        {
            int validatedInt;
            string input = null;
            string errorMessage = "";
            bool validInput = false;
            int maxYear = DateTime.Now.Year;
            do
            {
                Console.WriteLine(errorMessage);
                Console.Write(message);
                input = Console.ReadLine();
                if (Int32.TryParse(input, out validatedInt))
                {
                    if (validatedInt > minYear)
                    {
                        if (validatedInt <= maxYear)
                        {
                            validInput = true;
                        } else
                        {
                            errorMessage = $"\n\t{validatedInt} is later than this year! Please enter a year up until this year";
                        }
                    } else
                    {
                        errorMessage = $"\n\t{validatedInt} is to early! Please enter a later year";
                    }
                } else
                {
                    errorMessage = $"\n\tYou did not enter a valid year! Please try again";
                }
            } while (!validInput);
            return validatedInt;
        }

        public static bool GetBool(string message = "Please enter Y or N: ")
        {
            bool selectedOption = true;
            bool stillLookingForAValidResponse = true;
            string input = null;

            do
            {
                Console.Write(message);
                input = Console.ReadLine();

                switch (input.ToLower())
                {
                    case "y":
                    case "yes":
                    case "t":
                    case "true":
                        {
                            selectedOption = true;
                            stillLookingForAValidResponse = false;
                        }
                        break;
                    case "n":
                    case "no":
                    case "f":
                    case "false":
                        {
                            selectedOption = false;
                            stillLookingForAValidResponse = false;
                        }
                        break;
                }
            } while (stillLookingForAValidResponse);
            return selectedOption;
        }

        public static double GetDouble(string message = "Enter a number: ")
        {
            double validatedDouble;
            string input = null;

            do
            {
                Console.WriteLine(message);
                input = Console.ReadLine();

            } while (!Double.TryParse(input, out validatedDouble));

            return validatedDouble;
        }

        public static double GetDouble(double min, double max, string message = "Enter a number: ")
        {
            double validatedDouble;
            string input = null;

            do
            {
                Console.WriteLine(message);
                input = Console.ReadLine();
            } while (!(Double.TryParse(input, out validatedDouble) && (validatedDouble >= min && validatedDouble <= max)));

            return validatedDouble;
        }

        public static decimal GetDecimal(string message = "Enter a number: ", int decimalDigits = 2)
        {
            decimal validatedDecimal;
            string input = null;
            string errorMessage = "";
            bool validResponse = false;

            do
            {
                Console.WriteLine(errorMessage);
                Console.Write(message);
                input = Console.ReadLine();
                if (Decimal.TryParse(input, out validatedDecimal))
                    validResponse = true;
                else 
                    errorMessage = "\n\tERROR: You did not enter a valid number. Please try again.\n ";
                
            } while (!validResponse);

            return Math.Round(validatedDecimal, decimalDigits);
        }
        public static decimal GetPositiveDecimal(string message = "Enter a number: ", int maxValue = -1)
        {
            decimal validatedDecimal;
            string input = null;
            string errorMessage = "";
            bool validResponse = false;

            do
            {
                Console.WriteLine(errorMessage);
                Console.Write(message);
                input = Console.ReadLine();
                if (Decimal.TryParse(input, out validatedDecimal))
                    if (validatedDecimal >= 0)
                        if (maxValue == -1)
                            validResponse = true;
                        else
                        {
                            if (validatedDecimal <= maxValue)
                                validResponse = true;
                            else
                                errorMessage = $"\n\tERROR: The value needs to be lower than or equal to {maxValue}.\n";
                        }
                    else
                        errorMessage = "\n\tERROR: The value needs to be 0 or higher.\n";
                else
                    errorMessage = "\n\tERROR: You did not enter a valid number. Please try again.\n ";

            } while (!validResponse);

            return validatedDecimal;
        }

        public static float GetFloat(string message = "Enter a float: ")
        {
            float validatedFloat;
            string input = null;
            bool validInput = false;
            do
            {
                Console.Write(message);
                input = Console.ReadLine();
                if (float.TryParse(input, out validatedFloat))
                {
                    validInput = true;

                }
            } while (!validInput);

            return validatedFloat;
        }
        public static float GetFloat(float min, string message = "Enter a float: ")
        {
            float validatedFloat;
            string input = null;
            string errorMessage = "";
            bool validInput = false;
            do
            {
                Console.WriteLine(errorMessage);
                Console.Write(message);
                input = Console.ReadLine();
                if (float.TryParse(input, out validatedFloat))
                {
                    if (validatedFloat > min)
                        validInput = true;
                    else
                        errorMessage = "You did not enter a high enough value. Please try again.";

                }
            } while (!validInput);

            return validatedFloat;
        }

        public static int GetIntInArray(int[] intArray, string message = "Enter an integer: ")
        {
            int validatedInt;
            string input = null;

            do
            {
                Console.Write(message);
                input = Console.ReadLine();
            } while (!Int32.TryParse(input, out validatedInt) && intArray.Contains(validatedInt));

            return validatedInt;
        }

        public static string GetString(string message = "Please enter a string")
        {
            string input = null;
            string errorMessage = null;
            bool validResponse = false;

            do
            {
                Console.WriteLine(errorMessage);
                Console.Write(message);
                input = Console.ReadLine();
                if (!string.IsNullOrWhiteSpace(input))
                    validResponse = true;
                else
                    errorMessage = "\n\tERROR: You did not enter anything! Please try again.\n ";
            } while (!validResponse);

            return input;
            
        }

        
    }

}
