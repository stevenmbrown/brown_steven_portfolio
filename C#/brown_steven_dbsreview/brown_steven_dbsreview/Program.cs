﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;
using System.IO;

namespace brown_steven_dbsreview
{
    class Program
    {
        MySqlConnection _Conn = null;

        static void Main(string[] args)
        {
            Program instance = new Program();
            MySqlCommand sqlCommand;
            bool exitProgram = false;
            string userChoice; string userCity; string query;
            instance._Conn = new MySqlConnection();


            Console.Clear();
            Console.WriteLine("Welcome to our Weather program. After entering a city name, the weather for that current city will be displayed.");
            do
            {
                Console.Clear();
                userChoice = Utility.GenerateArrowMenu("-- Main Menu --\n", new string[] { "Check a city's weather", "Enter update temperature for a city", "Exit" });
                instance.Connect();
                switch (userChoice)
                {
                    case "Check a city's weather":
                        userCity = Validation.GetString("Please enter a city: ");
                        using (sqlCommand = new MySqlCommand($"SELECT COUNT(*) FROM weather WHERE city like \"{userCity}\"", instance._Conn))
                        {
                            if (Convert.ToBoolean(sqlCommand.ExecuteScalar()))
                            {
                                DataTable data = instance.QueryDB($"SELECT temp, pressure, humidity FROM weather WHERE city = \"{userCity}\" ORDER BY createdDate DESC LIMIT 1");
                                DataRowCollection rows = data.Rows;
                                foreach (DataRow row in data.Rows)
                                {
                                    Console.WriteLine($"City: {userCity}\n\tTemperature: {row["temp"]}\tPressure: {row["pressure"]}\tHumidity: {row["humidity"]}%");
                                }
                            }
                            else
                            {
                                Console.WriteLine("No Data Available for the selected city");
                            }

                        }
                        break;
                    case "Enter update temperature for a city":
                        query = instance.GenerateAddCityQuery();
                        using (sqlCommand = new MySqlCommand(query, instance._Conn))
                        {
                            MySqlDataReader myReader;
                            myReader = sqlCommand.ExecuteReader();
                            
                        }
                        Console.WriteLine("The data was added to our database!");
                        break;
                    default:
                        exitProgram = true;
                        Console.WriteLine("Thanks for using our program!");
                        break;
                }
                instance._Conn.Close();
                Utility.PressAnyKey();
            } while (!exitProgram);
           
        }

        string GenerateAddCityQuery()
        {
            string city = Validation.GetString("Please enter the city name: ");
            decimal temp = Validation.GetDecimal("Please enter the temperature: ", 3);
            decimal pressure = Validation.GetDecimal("Please enter the pressure: ", 3);
            int humidity = Validation.GetInt(0, 100, "Please enter the humidity (0 - 100): ");
            return $"INSERT INTO weather(city, createdDate, temp, pressure, humidity) VALUES (\"{city}\", \"{DateTime.Now.ToString("yyyy-MM-dd H:mm:ss")}\", {temp}, {pressure}, {humidity});";

        }
        void Connect()
        {
            BuildConnectionString();
            try
            {
                _Conn.Open();
                //Console.WriteLine("Connection Successful!");

            }
            catch (MySqlException e)
            {
                string msg = "";
                switch (e.Number)
                {
                    case 0:
                        {
                            msg = e.ToString();
                            break;
                        }
                    case 1042:
                        {
                            msg = "Can't resolve host address.\n" + _Conn.ConnectionString;
                            break;
                        }
                    case 1045:
                        {
                            msg = "Invalid Username or Password\n";
                            break;
                        }
                    default:
                        {
                            msg = e.ToString();
                            break;
                        }
                }
                Console.WriteLine(msg);
            }
        }

        void BuildConnectionString()
        {
            string ip = "";
            using (StreamReader sr = new StreamReader("C:\\VFW\\connect.txt"))
            {
                ip = sr.ReadLine();
            }
            string conString = $"Server={ip};";
            conString += "uid=dbsAdmin;";
            conString += "pwd=password;";
            conString += "database=SampleAPIData;";
            conString += "port=8889";
            _Conn.ConnectionString = conString;
        }

        DataTable QueryDB(string query)
        {
            MySqlDataAdapter adapter = new MySqlDataAdapter(query, _Conn);
            DataTable data = new DataTable();

            adapter.SelectCommand.CommandType = CommandType.Text;
            adapter.Fill(data);

            return data;

        }

    }
}
