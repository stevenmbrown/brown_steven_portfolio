﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace brown_steven_dbsreview
{
    class Utility
    {

        //These Methods Generate a Menu for the user

        public static int GenerateNumericIntMenu(string _Message, List<string> _MenuOptions)
        {
            string userChoice; string menuText = ""; string errorMessage = null;
            bool validEntry = false;
            int[] menuNumbers = new int[_MenuOptions.Count];
            int validatedChoice = 0; int userChoiceInt;
            // Generate Menu
            for (int i = 0; i < _MenuOptions.Count; i++)
            {
                menuNumbers[i] = i + 1; // So menu starts numbering at 1 instead of 0
                menuText += $"\t" + menuNumbers[i] + ")  " + _MenuOptions[i] + "\n";
            }
            menuText += "\n\tSelection: ";
            do
            {
                Console.Clear();
                Console.WriteLine(_Message);
                if (errorMessage != null)
                    Console.WriteLine($"\tERROR: {errorMessage}");
                Console.Write(menuText);
                userChoice = Console.ReadLine();
                if (int.TryParse(userChoice, out userChoiceInt))
                {
                    if (menuNumbers.Contains(userChoiceInt))
                    {
                        validatedChoice = userChoiceInt - 1; // -1 because we start at 1 instead of 0
                        validEntry = true;
                    }
                }
                else
                {
                    string[] lowerMenuOptions = _MenuOptions.Select(s => s.ToLowerInvariant()).ToArray();
                    if (lowerMenuOptions.Contains(userChoice.ToLower()))
                    {
                        int index = Array.FindIndex(lowerMenuOptions, row => row.Contains(userChoice.ToLower()));
                        validatedChoice = index;
                        validEntry = true;
                    }
                }
                if (!validEntry)
                    errorMessage = "You did not enter a valid option. Please try again.\n";
            } while (!validEntry);
            return validatedChoice;   
        }

        public static int GenerateNumericIntMenu(string _Message, string[] _MenuOptions)
        {
            string userChoice; string menuText = ""; string errorMessage = null;
            int[] menuNumbers = new int[_MenuOptions.Length];
            int validatedChoice = 0; int userChoiceInt;
            bool validEntry = false;
            // Generate Menu
            for (int i = 0; i < _MenuOptions.Length; i++)
            {
                menuNumbers[i] = i + 1; // So menu starts numbering at 1 instead of 0
                menuText += $"\t" + menuNumbers[i] + ")  " + _MenuOptions[i] + "\n";
            }
            menuText += "\n\tSelection: ";
            do
            {
                Console.Clear();
                Console.WriteLine(_Message);
                if (errorMessage != null)
                    Console.WriteLine("\tERROR: " + errorMessage);
                Console.Write(menuText);
                userChoice = Console.ReadLine();
                if (int.TryParse(userChoice, out userChoiceInt))
                {
                    if (menuNumbers.Contains(userChoiceInt))
                    {
                        validatedChoice = userChoiceInt - 1; // -1 because we start at 1 instead of 0
                        validEntry = true;
                    }
                }
                else
                {
                    string[] lowerMenuOptions = _MenuOptions.Select(s => s.ToLowerInvariant()).ToArray();
                    if (lowerMenuOptions.Contains(userChoice.ToLower()))
                    {
                        int index = Array.FindIndex(lowerMenuOptions, row => row.Contains(userChoice.ToLower()));
                        validatedChoice = index;
                        validEntry = true;
                    }
                }
                if (!validEntry)
                    errorMessage = "You did not enter a valid option. Please try again.\n";
            } while (!validEntry);
            return validatedChoice;
        }

        public static string GenerateArrowMenu(string _Message, string[] _MenuOptions, int xPos = 0, int yPos = 0)
        {
            int currentOption = 0;
            ConsoleKeyInfo key;
            do
            {
                Console.SetCursorPosition(xPos, yPos);
                Console.WriteLine(_Message);
                for (int s = 0; s < _MenuOptions.Length; s++)
                {
                    if (s == currentOption)
                        Console.Write(">> ");
                    else
                        Console.Write("   ");
                    Console.WriteLine(_MenuOptions[s]);
                }
                key = Console.ReadKey(true);
                if (key.Key.ToString() == "DownArrow")
                {
                    currentOption++;
                    if (currentOption == _MenuOptions.Length)
                        currentOption = 0;
                }
                if (key.Key.ToString() == "UpArrow")
                {
                    currentOption--;
                    if (currentOption < 0)
                        currentOption = _MenuOptions.Length - 1;
                }
            } while (key.KeyChar != 13);
            return _MenuOptions[currentOption];
        }

        public static string GenerateNumericMenu(string _Message, string[] _MenuOptions)
        {
            string userChoice;
            bool validEntry = false;
            int[] menuNumbers = new int[_MenuOptions.Length];
            string menuText = "";
            string validatedChoice = ""; int userChoiceInt;
            string _ErrorMessage = "";
            // Generate Menu
            for (int i = 0; i < _MenuOptions.Length; i++)
            {
                menuNumbers[i] = i + 1; // So menu starts numbering at 1 instead of 0
                menuText += $"\t" + menuNumbers[i] + ")  " + _MenuOptions[i] + "\n";
            }
            menuText += "\n\tSelection: ";
            do
            {
                Console.Clear();
                Console.WriteLine(_Message);
                if (_ErrorMessage != "")
                    Console.WriteLine("\tERROR: " + _ErrorMessage);
                Console.Write(menuText);
                userChoice = Console.ReadLine();
                if(int.TryParse(userChoice, out userChoiceInt)) {
                    if(menuNumbers.Contains(userChoiceInt)) {
                        validatedChoice = _MenuOptions[userChoiceInt - 1]; // -1 because we start at 1 instead of 0
                        validEntry = true;
                    } 
                } else
                {
                    string[] lowerMenuOptions = _MenuOptions.Select(s => s.ToLowerInvariant()).ToArray();
                    if (lowerMenuOptions.Contains(userChoice.ToLower())) {
                        int index = Array.FindIndex(lowerMenuOptions, row => row.Contains(userChoice.ToLower()));
                        validatedChoice = _MenuOptions[index];
                        validEntry = true;
                    }
                }

                if (!validEntry)
                    _ErrorMessage = "You did not enter a valid option. Please try again.\n";
            } while (!validEntry);

            return validatedChoice;

        }



        public static void PressAnyKey(string _Message = "\n\tPress any key to continue.")
        {
            Console.WriteLine(_Message);
            Console.ReadKey();
        }

    }
}
