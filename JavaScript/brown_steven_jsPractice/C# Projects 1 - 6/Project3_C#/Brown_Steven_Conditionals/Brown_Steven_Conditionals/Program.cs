﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brown_Steven_Conditionals
{
    class Program
    {
        static void Main(string[] args)
        {
            /* 
             Steven Brown
             SDI Section 1
             October 1, 2017
             SDI Conditionals Assignment

             Problem #1: Temperature Converter
             Problem Description: For this problem, the user will input a numeric value which we will save as a double to account for decimal values (85.5). We won't use
             the decimal data type because we aren't dealing with currency. After the user inputs the numeric value, we will prompt the user whether or not the value entered
             is in Fahrenheit or Celsius. The user will be requested to enter a F for Fahrenheit or a C for Celsius. After the user inputs either F or C, the data is computed
             and the temperature conversion is displayed.
            */
            double temperatureValue; //First we start by initilizing a variable for the temperature value
            //Then we will continue by welcoming the user to our program
            Console.WriteLine("Hello and welcome to Steven Brown's Conditional Program! We have a whole bunch of problems that have been solved to make your life easier!");
            //Next we will introduce the user to our Temperature converter program and request the user to input a number.
            Console.WriteLine("To start off, we have created a Temperature Converter so you can convert Fahrenheit to Celsius and vice versa!\n\rTo begin, please enter a numeric temperature value!");
            //We asked the user for a number, so now we have to read, convert and store it as such!
            while (!double.TryParse(Console.ReadLine(), out temperatureValue) || string.IsNullOrEmpty(temperatureValue.ToString()))//For the sake of being efficient (and neat!) we attempt to parse the user input into a double as soon as we got it and store it right away in a variable. Then we ensure it wasn't an empty string before moving on
            {
                Console.WriteLine("You did not enter a valid temperature value! Please enter a numeric temperature value!"); //Here we let the user know what they did wrong
            }

            //Next we ask the user whether the numeric value was in Celcius or Fahrenheit
            Console.WriteLine("Great! Now was that numeric temperature value in Fahrenheit (F) or Celsius (C)? Please enter F for Fahrenheit or C for Celsius.");
            //Ask and you shall receive! Let's accept the input and store it
            string measurement = Console.ReadLine();
            while (string.IsNullOrEmpty(measurement) || (measurement[0] != 'c' && measurement[0] != 'C' && measurement[0] != 'f' && measurement[0] != 'F')) //Now we check to see if the user's input was valid. We increase the likelyhood of accepting the user's input by examining just the first character of the user inputted string
            {
                Console.WriteLine("You did not enter a valid input, please enter C for Celsius, or F for Fahrenheit! Please enter your choice now: "); //Let the user know what they did wrong
                measurement = Console.ReadLine();
            }
            /* 
             Now we will be getting into the if statements and processing the data and displaying the output. Instead of processing the measurement string solely based on if a C, c, F, or f was entered, 
             we are going to account for the chance that maybe the user did not fully read the instructions and typed in Celsius, of fahreneheit. This can easily be done by examining the first character of the 
             string (which is essentially an Array of characters). So first things first, did the user input a Celsius value? 
            */
            if (measurement[0] == 'C' || measurement[0] == 'c') //Here we are checking the first character of the measurement, and if it's a capital or lowercase c, the user inputed Celsius
            {
                double fahrenheit = Math.Round(((temperatureValue * 1.8) + 32), 2); //Here we used the formula to convert Celsius into Fahrenheit, and then we run the result through Math.Round() to round the answer to two decimal places
                Console.WriteLine("The temperature is " + fahrenheit + " degrees Fahrenheit."); // Finally we output the results

            }
            else //Next, if the user didn't want Celsius, and we already confirmed that they either entered F or f, then we know that the user inputted Fahrenheit
            {
                double celsius = Math.Round(((temperatureValue - 32) * (5.0 / 9)), 2);  //Here we used the formula to convert Fahrenheit into Celsius and again run the result through Math.Round() to round the answer to two decimal places
                Console.WriteLine("The temperature is " + celsius + " degrees Celsius"); //Now that we have the Celsius value we can output 

            }

            /* 
             Test Values
             Iteration 1
             temperatureValue = 32, measurement = F
             The temperature is 0 degrees Celsius

            Iteration 2
            temperatureValue = 100, measurement = C
            The temperature is 212 degrees Fahrenheit

            Iteration 3
            temperatureValue = 50, measurement = c
            The temperature is 122 degrees Fahrenheit

            Iteration 4
            temperatureValue = 0, measurement = f
            The temperature is -17.78 degrees Celsius

            Iteration 5
            temperatureValue = 0, measurement = Celsius
            The temperature is 32 degrees Fahrenheit
           */

            /* 
             Problem #2: Last Chance for Gas
             Problem Description: A driver needs to determine if he or she can make it to the next gas station which in 200 miles. We need to accept some
             data which includes how many gallons the car tank can hold, how full is the gas tank, and how many miles per gallon the car gets. 

            So let's start by initilizing some variables, welcoming the user to Part 2, and prompting for how many gallons of gas their car can hold
            */

            double totalTankGallons; //We will save the user input as a double just in case he or she gets crazy with the amount, like 'My car holds 25.35 gallons of gas'. We are converting the string to a double here.
            double gasPercentage; //Again, saved as a double in case the car has 25.35% of gas left.
            double milesPerGallon; //Keeping it consistent with the double.

            Console.WriteLine("Welcome to Part 2 of Steven Brown's Conditional Program!\n\rThis portion of the program is designed to determine whether or not you need to gas up now or can make it 200 miles across the desert to the next gas station. To determine this, we will need some information from you!\n\r\n\rFirst things first, how many gallons can your vehicle tank hold?");
            while (!double.TryParse(Console.ReadLine(), out totalTankGallons) || totalTankGallons <= 0) //Here we ensure that the user did in fact enter a numeric value,  and it's a valid value because its not 0 or a negative number (You gas tank can't hold negative gallons!)
            {
                Console.WriteLine("You did not enter a valid value for the total amount of gallons your gas tank can hold! Please try again. How many gallons can your gas tank hold?"); //Here we let the use know we caught them in the act of messing up!
            }
               

            Console.WriteLine("Great! Now we need to know what percentage of the tank has gas. Please enter between 0 - 100. (Your tank can't hold 0 gallons!)");  //Next we prompt for the percentage of gas in the tank
            while(!double.TryParse(Console.ReadLine(), out gasPercentage) || gasPercentage <= 0 || gasPercentage > 100) //Here we are checking that the user inputted a number that is greater than or equal to 0 up to 100 (since the tank can only be 0 - 100% full). If they didn't put in a valid input, reprompt them
            {
                Console.WriteLine("You did not enter a number between 0 and 100. Please try again and enter a number between 0 - 100! (Your tank can't hold 0 gallons!)");
            }

            Console.WriteLine("Awesome! How many miles per gallon does your vehicle get? (Must be greater than 0)"); //Finally we prompt for the miles per gallon the vehicle gets
            while(!double.TryParse(Console.ReadLine(), out milesPerGallon) || milesPerGallon <= 0) //Once again ensuring that the input is valid by making sure its a positive number. If it's not valid, reprompt
            {
                Console.WriteLine("You did not enter a valid number for how many miles per gallon your vehicle gets. Please try again. How many miles per gallon does your vehicle get?");
            }

            //Compute the remaining miles 
            double remainingMiles = (totalTankGallons * (gasPercentage / 100)) * milesPerGallon;

            if (remainingMiles >= 200) //Determine if the remaining miles is greater than 200 meaning the user can successfully make it across the desert
            {
                Console.WriteLine("Yes, you can drive " + remainingMiles + " more miles and you can make it without stopping for gas!");
            }
            else //Unfortunately the opposite of making it across the desert is not being able to make it across the desert, so we advise the user on how many miles they have left and recommend to gas up now.
            {
                Console.WriteLine("You only have " + remainingMiles + " miles you can drive, better get gas now while you can!");
            }

            /* 
             Test Values
             Iteration 1
             totalTankGallons = 20, gasPercentage = 50, milesPerGallon = 25
             Yes, you can drive 250 more miles and you can make it without stopping for gas!

             Iteration 2
             totalTankGallons = 12, gasPercentage = 60, milesPerGallon = 20
             You only have 144 miles you can drive, better get gas now while you can!

             Iteration 3
             totalTankGallons = 15, gasPercentage = 1, milesPerGallon = 100
             You only have 15 miles you can drive, better get gas now while you can!

            */

            /* 
             Problem #3: Grade Letter Calculator
             Problem Description: A user inputs an integer between 0 and 100 that denotes his or her average for the class. We need to determine the appropriate letter grade
             for the user based on the user input provided. 

            So let's start by welcoming the user to Part 3, initilize some variables, and prompting for the user's numeric grade
            */
            Console.WriteLine("Welcome to Part 3 of Steven Brown's Conditional Program!\n\rThis portion of the program is designed to determine your appropriate letter grade based on the numeric integer grade you provide!");

            char grade; //We decided to use a character data type here because the letter grade is only one letter for this assignment. We could have also used a string.
            int average; //Since the user is requested to put in a whole number, the input can be saved as an integer (after being converted from a string of course!)

            Console.WriteLine("\n\rPlease enter your numeric grade which should be a whole number between 0 and 100."); //Prompt for input!

            while(!int.TryParse(Console.ReadLine(), out average) || average < 0 || average > 100) //Here we are ensuring that the user inputted a whole number that is between 0 and 100, and we store it in the average variable
            {
                Console.WriteLine("\n\rYou didn't enter a number between 0 and 100! Please try again. Please enter your numeric grade which should be a whole number between 0 and 100.");
            }
          
            /* 
             Now we go down the line corresponding numeric value to its appropriate letter grade. If the numeric value inputted matches the stipulations for the letter grade, the value for 
             char grade is set to that letter.
            */
            if (average >= 90 && average <= 100)
            {
                grade = 'A';
            }
            else if (average >= 80 && average < 90)
            {
                grade = 'B';
            }
            else if (average >= 73 && average < 80)
            {
                grade = 'C';
            }
            else if (average >= 70 && average < 73)
            {
                grade = 'D';
            }
            else
            { // Since we already determined that the average is between 0 and 100 and that everything 70 - 100 is accounted for, that leaves 0 - 69 left as possible values for average meaning the user has an F
                grade = 'F';
            }

            //Now for the moment of truth. We output the numeric value the user gave us and the letter grade we found associated with it!
            Console.WriteLine("You have a " + average + "%, which means you have earned a(n) " + grade + " in the class!");

            /* 
             Test Values
             Iteration 1
             average = 92
             You have a 92%, which means you have earned a(n) A in the class!

             Iteration 2
             average = 80
             You have a 80%, which means you have earned a(n) B in the class!

             Iteration 3
             average = 67
             You have a 67%, which means you have earned a(n) F in the class!

             Iteration 4
             average = 73
             You have a 73%, which means you have earned a(n) C in the class!

             Iteration 5
             average = 120
             You didn't enter a number between 0 and 100! Please try again. Please enter your numeric grade which should be a whole number between 0 and 100.
            */

            /* 
             Problem #4: Discount Double Check
             Problem Description: The user is going to be purchasing 2 items from an online store, 
             and if he or she spends 100 or more, the user will receive a 10% discount. If the user 
             spends between 50 to 99 dollars, he or she receives a 5% discount. If the user spends less than $50, 
             there will be no discount.

            So let's start by initilizing the variables and welcoming the user to Part 4
            */
            decimal firstItem; //Since we are dealing with money, we chose to use the decimal data type for both of these variables. 
            decimal secondItem;

            Console.WriteLine("Welcome to Part 4 of Steven Brown's Conditional Program!\n\rThis portion of the program is designed to determine whether you are getting a discount on your two item purchase and for how much!");
            //Here we prompt the user for the cost of the first item
            Console.WriteLine("To start, please enter the cost of your first item!");
            while(!decimal.TryParse(Console.ReadLine(), out firstItem) || firstItem <= 0) // Here we check to ensure that the user inputed a valid numeric value and that it's greater than 0
            {
                Console.WriteLine("The first item cost needs to be a number great than 0! Please try again, what is the cost of your first item?");
            }


            Console.WriteLine("Thank you, now please enter the cost of your second item!"); //Moving on and asking for the cost of the second item
            while (!decimal.TryParse(Console.ReadLine(), out secondItem) || secondItem <= 0) // Again we check to ensure that the user inputed a valid numeric value and that it's greater than 0
            {
                Console.WriteLine("The second item cost needs to be a number great than 0! Please try again, what is the cost of your second item?");
            }

            decimal totalPurchase = firstItem + secondItem; //Now we need the total between the two items to determine if the user qualified for any discount
            if (totalPurchase < Convert.ToDecimal(50)) //If the totalPurchase was less than $50, the user did not qualify for any discount
            {
                Console.WriteLine("Your total purchase is " + string.Format("{0:C}", totalPurchase) + "."); //We still format the totalPurchase so it displays like currency

            }
            else //If we made it to this point, we determined the user spent $50 or more so he or she qualified for a discount.
            {
                if (totalPurchase < Convert.ToDecimal(100)) //Now to narrow it down, did the user spend less than $100? If so, the user gets a 5% discount.
                {
                    //So we multiply the totalPurchase by 0.95 (essentially accounting for the 5% discount, or 0.05) and then formatting it so it outputs like currency.
                    Console.WriteLine("Your total purchase is " + string.Format("{0:C}", (totalPurchase * Convert.ToDecimal(0.95))) + ", which includes your 5% discount."); 
                }
                else
                {
                    //Much like the 5% discount, in this instance, we multiply the totalPurchase by 0.90 to account for the 10% discount and then format the result so it outputs like currency.
                    Console.WriteLine("Your total purchase is " + string.Format("{0:C}", (totalPurchase * Convert.ToDecimal(0.90))) + ", which includes your 10% discount.");
                }
            }
            /* 
             Test Values
             Iteration 1
             firstItem = 45.50, secondItem = 75.00
             Your total purchase is $108.45, which includes your 10% discount.

             Iteration 2
             firstItem = 30.00, secondItem - 25.00
             Your total purchase is $52.25, which includes your 5% discount.

             Iteration 3
             firstItem = 5.75, secondItem - 12.50
             Your total purchase is $18.25.

             Iteration 4
             firstItem = 5.00, secondItem - 5.00
             Your total purchase is $10.00.

            */
        }
    }
}
