﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brown_Steven_CustomClass
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Steven Brown
             October 9, 2017
             SDI Section 1
             Assignment Custom Class (Hero)
            */

            //First we need to declare some variables!

            double startingHP = 500; //We saved the starting HP into a variable so we can easily change it
            int option; //This variable will hold the user inputted option for which direction the health changes
            string optionName; //This gives a string value to the user's option (either increase, or decrease)
            double healthChange; //This variable will hold the user inputted number that represents how much the Hero's health changed!
            double heroHPChange; //This variable will store how much HP was actually changed (incase the hero maxed his health or died!)

            //Now Let's instantiate our Hero!

            Hero ourHero = new Hero(startingHP, startingHP); //We chose to pass the startHP as the first 2 arguements and let the _MinHP resort to it's default value (0 as per the assignment requirements)

            //Now our user deserves a Heroic welcome! Let's inform them on how the program will work!
            Console.WriteLine("\n\rWelcome to Steven Brown's Custom Class (Hero) Program! In this program, we have created your virtual hero who starts with {0} HP! For the remainder of the program, we will prompt you whether the health went up or down " +
                "and by how much. During the first prompt we ask you whether the HP went up or down, enter the number 1 for Up or the number 2 for down! After that, you will be prompted by how much. Feel free to be as specific as possible (get into the decimal places" +
                " if your heart so desires! So let's get started!", startingHP); 

            while(ourHero.GetCurrentHP() > ourHero.GetMinHP())  //As per the guidance give to me by Mr. Williams, the loop is to end when the Hero dies (AKA when the Hero's current Health equals minHP)
            {
                //Now let's ask the user whether the Hero's health went up or down. The number 1 will correspond to Up, and 2 will correspond to Down
                Console.Write("\n\rWhat direction did the Hero's health change?\n\r1) Enter the number 1 if the Hero's health went up!\n\r2) Enter the number 2 if the Hero's health went down\n\rPlease enter the number that corresponds to which direction the Health changed: ");
                while(!int.TryParse(Console.ReadLine(), out option) || (option != 1 && option != 2)) //Here we run a loop until the user inputs a valid option (either 1 or 2)
                {
                    Console.WriteLine("You did not input a valid option! Please input one of the two possible options!"); //If the program reaches this point, it means the user did not input either 1 or 2 and needs to be reprompted for an option!
                }

                //So now we know that the user inputted either 1 or 2 so we can move on accordingly
                if(option == 1) //If the user inputted 1
                {
                    optionName = "increase";  //We set the optionName (which is a string) as "increase". This is more to make our user requests more appropriate
                } else //Otherwise the user inputted 2
                {
                    optionName = "decrease"; //So we set the optionName to "decrease"
                }

                //Now we can use the previously set optionName to prompt the user, how much did the Hero's HP increase / decrease?
                Console.Write("\n\rNow how much did the Hero's HP {0}? ", optionName);
                while(!double.TryParse(Console.ReadLine(), out healthChange) || healthChange < 0) //The value can be any positive number. Since we prompt whether the health is increasing or decreasing, we will not allow negative numbers
                {
                    Console.WriteLine("Please enter a valid number that is greater than or equal to 0!"); //The user did not input a valid number greater than 0 so they need to be reprompted!
                }

                //We now have all the information we need and can call our custom method to change the Hero's HP. We store how much HP was actually changed in the variable heroHPChange
                heroHPChange = ourHero.ChangeCurrentHP(option, healthChange);

                if(ourHero.GetCurrentHP() == ourHero.GetMaxHP() && option == 1) //If our hero's HP Maxed out, append a sentence. We check to see if the last change was an increase, because if it was a decrease for 0, we shouldn't say that maxed out the Hero's health!
                {
                    Console.Write("That last increase maxed out our Hero's health! Our Hero could only recover {0} more health! ", heroHPChange);
                }
                if(ourHero.GetCurrentHP() <= ourHero.GetMinHP()) //If our hero's HP reach minHP, append a sentence 
                {
                    Console.Write("Lethal blow! Our Hero could only take {0} more damage! ", heroHPChange);
                }
                //Finally, we can update the user on the Hero's latest stats before the program moves on to the next iteration!
                Console.WriteLine("Our Hero's HP {0}d by {1}, and now has {2} HP!", optionName, heroHPChange, ourHero.GetCurrentHP());
            }
            //Finish off the program letting the user know the Hero died, but they can always play again!
            Console.WriteLine("Our hero has been slained! But not in vain! Re-run the program to avenge our Hero's death!");

            /*
            Test Results

            Welcome to Steven Brown's Custom Class (Hero) Program! In this program, we have created your virtual hero who starts with 500 HP! For the remainder of the program, we will prompt you whether the health went up or down and by how much. During the first prompt we ask you whether the HP went up or down, enter the number 1 for Up or the number 2 for down! After that, you will be prompted by how much. Feel free to be as specific as possible (get into the decimal places if your heart so desires! So let's get started!

            What direction did the Hero's health change?
            1) Enter the number 1 if the Hero's health went up!
            2) Enter the number 2 if the Hero's health went down
            Please enter the number that corresponds to which direction the Health changed: 1

            Now how much did the Hero's HP increase? 20
            That last increase maxed out our Hero's health! Our Hero could only recover 0 more health! Our Hero's HP increased by 0, and now has 500 HP!

            What direction did the Hero's health change?
            1) Enter the number 1 if the Hero's health went up!
            2) Enter the number 2 if the Hero's health went down
            Please enter the number that corresponds to which direction the Health changed: 2

            Now how much did the Hero's HP decrease? 450
            Our Hero's HP decreased by 450, and now has 50 HP!

            What direction did the Hero's health change?
            1) Enter the number 1 if the Hero's health went up!
            2) Enter the number 2 if the Hero's health went down
            Please enter the number that corresponds to which direction the Health changed: 3
            You did not input a valid option! Please input one of the two possible options!
            -1
            You did not input a valid option! Please input one of the two possible options!
            a
            You did not input a valid option! Please input one of the two possible options!

            You did not input a valid option! Please input one of the two possible options!
            2

            Now how much did the Hero's HP decrease? 25.5
            Our Hero's HP decreased by 25.5, and now has 24.5 HP!

            What direction did the Hero's health change?
            1) Enter the number 1 if the Hero's health went up!
            2) Enter the number 2 if the Hero's health went down
            Please enter the number that corresponds to which direction the Health changed: 1

            Now how much did the Hero's HP increase? a
            Please enter a valid number that is greater than or equal to 0!

            Please enter a valid number that is greater than or equal to 0!
            485.5
            That last increase maxed out our Hero's health! Our Hero could only recover 475.5 more health! Our Hero's HP increased by 475.5, and now has 500 HP!

            What direction did the Hero's health change?
            1) Enter the number 1 if the Hero's health went up!
            2) Enter the number 2 if the Hero's health went down
            Please enter the number that corresponds to which direction the Health changed: 2

            Now how much did the Hero's HP decrease? 550
            Lethal blow! Our Hero could only take 500 more damage! Our Hero's HP decreased by 500, and now has 0 HP!
            Our hero has been slained! But not in vain! Re-run the program to avenge our Hero's death!
            Press any key to continue . . .
            */

        }
    }
}
