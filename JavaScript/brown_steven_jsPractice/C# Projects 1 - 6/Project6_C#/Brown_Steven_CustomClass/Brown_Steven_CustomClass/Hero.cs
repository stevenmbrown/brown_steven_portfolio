﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brown_Steven_CustomClass
{
    class Hero
    {
        /*
         Steven Brown
         October 9, 2017
         SDI Section 1
         Assignment Custom Class (Hero)
        */

        //First we can start with our properties (AKA member variables)

        double mMaxHP; // Highest hit points the character can have
        double mMinHP; //Lowest number of hit points
        double mCurrentHP; //Current HP

        //Next we create our constructor method which should have the same name as our class
        public Hero(double _maxHP, double _currentHP, double _minHP = 0) //We follow our suggested naming convention of using an underscore before passed variables. We also allow for the passing of _MinHP, but if it is not passed, it is defaulted to 0.
        {
            mMaxHP = _maxHP; //Set the mMaxHP property to the passed value
            mCurrentHP = _currentHP; //Set the mCurrentHP property to the passed value
            mMinHP = _minHP; //As per the assignment, the minimum HP is 0
        }

        //Now let's create our getter methods 

        public double GetMaxHP() //Returns the mMaxHP value (which is a double)
        {
            return this.mMaxHP;
        }

        public double GetMinHP() //Returns the mMinHP value (which is also a double)
        {
            return this.mMinHP;
        }

        public double GetCurrentHP() //Returns the mCurrentHP value (which is a double)
        {
            return this.mCurrentHP;
        }

        // Now let's create our setter methods 

        public void SetMaxHP(double _newMaxHP) //Sets the value of mMaxHP to the passed value
        {
            this.mMaxHP = _newMaxHP; 
        }

        public void SetMinHP(double _newMinHP) //Sets the value of mMinHP to the passed value
        {
            this.mMinHP = _newMinHP;
        }

        public void SetCurrentHP(double _newCurrentHP) //Sets the value of the mCurrentHP to the passed value
        {
            this.mCurrentHP = _newCurrentHP;
        }

        //Now for our custom method that changes the Hero's HP when the Hero receives damage or restores health
        public double ChangeCurrentHP(int _direction, double _change) //Our method returns a double and takes two parameters, one for the direction (if the HP is increasing or decreasing) and the amount it is changing by
        {
            double actualChange;
            if (_direction == 1) //If the direction is up (HP is increasing)
            {
                if (this.mCurrentHP + _change <= this.mMaxHP) //If the sum of the Hero's current HP + the change to HP is less than the mMaxHP 
                {
                    actualChange = _change;
                    this.mCurrentHP += _change; //Allow the calculation
                }
                else //Otherwise, set the mCurrentHP to the mMaxHP (This ensures the Hero never get's more than Max HP)
                {
                    actualChange = this.mMaxHP - this.mCurrentHP; //Figure out the difference between mMaxHP and mCurrentHP to return at the end of the method
                    this.mCurrentHP = this.mMaxHP; //Set mCurrentHP to mMaxHP
                }
            }
            else //If the direction is not up, it must be down! (Decreasing)
            {
                if (this.mCurrentHP - _change >= this.mMinHP) //If the Hero's current HP minus the change in HP is greater than mMinHP (for this assignment it's 0) then allow the full _change
                {
                    actualChange = _change;
                    this.mCurrentHP -= _change; //Allow the calculation
                }
                else //Otherwise, set mCurrentHP to 0 (This ensures the Hero's health is never below the mMinHP [or 0 in this assignment])
                {

                    actualChange = this.mMinHP + this.mCurrentHP; //Figure out the sum between mMinHP and mCurrentHP which will give you the difference
                    this.mCurrentHP = this.mMinHP; //Set mCurrentHP to mMinHP
                }
            }
            return actualChange; //This will return how much the HP actually changed
        }
    }
}
