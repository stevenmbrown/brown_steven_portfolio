﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brown_Steven_CountFish
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Steven Brown
             October 2, 2017
             SDI Section 1
             Count The Fish Assignment
            */

            /*
             First thing we need to do is to declare our Array as well as some other variables. The fish Array will contain all the fish in our fish tank
             and since we are saving them as strings, the array needs to be the string data type. The values inside are provided by the assignment instructions.
            */
            string[] fish = new string[] { "red", "blue", "green", "yellow", "blue", "green", "blue", "blue", "red", "green" }; //We could have also omitted 'new string[]' here when initilizing the Array
            int input; //This will be the variable that we store the user inputted option in. Since the possible options are 1 - 4, we will use the data type int
            int count = 0; //This will store the count of fish of a specified color when the loops kick off. This again will be a whole number so int should work fine
            string fishColor; //This will store the string value of the user inputted choice. 
            
            // Now let's welcome the user to the program, display the menu, and prompt the user for input.
            Console.WriteLine("Welcome to Steven Brown's Wonderful World of Fish Program! We've got fish for days! Want to know how many? Ask us by selecting an option!" +
                "\n\r\n\r1) Enter the number 1 to see how many Red Fish are in our tank!\n\r2) Enter the number 2 to see how many Blue Fish are in our tank!\n\r3) Enter the number 3 to see how many Green fish are in our tank!" +
                "\n\r4) Enter the number 4 to see how many Yellow Fish are in our tank!\n\r");
            Console.Write("Please enter your option: ");

            /* 
             Next we will use a while loop to validate the user input. First the loop will try to parse the user input as an int, and if it is a valid integer, it will save it to the variable
             input. Next, it will check to ensure that the inputted value is either 1,2,3, or 4 (the only valid options for this program). If the input is not a valid integer, or is not a valid option,
             the user is notified that they did not input a valid option and reprompts them for a correct response. 
           */
            while (!int.TryParse(Console.ReadLine(), out input) || (input != 1 && input != 2 && input != 3 && input != 4)) { 
                Console.WriteLine("You did not enter a valid option! Please only enter the number of the option you want to choose!\n\r\n\r1) Enter the number 1 to see how many Red Fish are in our tank!\n\r2) Enter the number 2 to see how many Blue Fish are in our tank!\n\r3) Enter the number 3 to see how many Green fish are in our tank!" +
                "\n\r4) Enter the number 4 to see how many Yellow Fish are in our tank!\n\r");
                Console.Write("Please enter your option: ");
            }

            /*
             Now we will start computing the data. First thing we do is determine what the input value is (1 equates to red, 2 equates to blue, 3 equates to green, and 4 equates to yellow)
             So the first if statement we use is 'is the input 1 (for red)'. If it is, we set the fishColor string to "red" and then start processing the Array. We use are foreach loop which
             takes each string inside of the array one by one and temporarily stores it in the string fishy. Then we check if the string we temporarily saved in fish matches that in fishColor,
             and if it is, increase our count variable by 1. If those two strings don't match, the foreach loop continues on until the end of the Array. This also ends our if statement where we 
             checked if the user inputted 1 (for red). Now we need to apply these same concepts for each possible option so moving sequentially we use a conditional statement to see if the user
             inputted 2 (for blue) and if they did, we apply the same process we storing the fishColor variable with the color (blue this time) and running through the Array with a foreach comparing
             each string with the fishColor variable. We again repeat this process for the inputs 3 (for green) and 4 for (yellow)
            */
            if (input == 1) //For red
            {
                fishColor = "red"; //We designed this program so that option 1 equates to "red" (and it was specificed in the instructions) so we set that here. 
                foreach (string fishy in fish) //For each string in the fish Array, we go through one at a time and store the string in the variable fishy
                {
                    if (fishy == fishColor) //If the string from the fish Array stored in fishy is the same of that stored in fishColor, then it's a match!
                    {
                        count++; //Shorthand for count = count + 1;
                    }
                }
            } 
            else if(input == 2) //For blue
            {
                fishColor = "blue"; //Again this was determined by the options we presented at the start 
                foreach (string fishy in fish)
                {
                    if (fishy == fishColor)
                    {
                        count++;
                    }
                }
            }
            else if(input == 3) //For green
            {
                fishColor = "green"; //Determined by the options at the start menu
                foreach (string fishy in fish)
                {
                    if (fishy == fishColor)
                    {
                        count++;
                    }
                }
            }
            else //Since we already determined that the user inputted 1,2,3, or 4 with the while loop, and we already checked to see if the user inputted 1, 2, and 3, and we determined it wasn't any of those, that leaves 4 as the only possible value
            {
                fishColor = "yellow"; //And once more determined by the start menu
                foreach (string fishy in fish)
                {
                    if (fishy == fishColor)
                    {
                        count++;
                    }
                }
            }

            //Now that the computations are done and we have the count value stored as well as the user's inputted fish color, we can print that information to the Console and end the program.

            Console.WriteLine("In the fish tank there are " + count + " fish of the the color " + fishColor + ".");

            /* 
             Test Results
             Iteration 1
             option = 1
             In the fish tank there are 2 fish of the the color red.

             Iteration 2
             option = 2
             In the fish tank there are 4 fish of the the color blue.

             Iteration 3
             option = 3
             In the fish tank there are 3 fish of the the color green.

             Iteration 4
             option = 4
             In the fish tank there are 1 fish of the the color yellow.

             Iteration 5
             option = 6
             You did not enter a valid option! Please only enter the number of the option you want to choose!

             1) Enter the number 1 to see how many Red Fish are in our tank!
             2) Enter the number 2 to see how many Blue Fish are in our tank!
             3) Enter the number 3 to see how many Green fish are in our tank!
             4) Enter the number 4 to see how many Yellow Fish are in our tank!

             Please enter your option:

             Other tests:
             Our code still works if we add more fish to the tank (as long as we add them 
             directly to the Array, and they are of colors we already have, and are written in lowercase)
            */

        }
    }
}
