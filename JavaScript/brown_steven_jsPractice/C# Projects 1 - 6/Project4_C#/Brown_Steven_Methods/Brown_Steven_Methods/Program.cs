﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brown_Steven_Methods
{
    class Program
    {
        static void Main(string[] args)
        {
            /* 
             Steven Brown
             October 6, 2017
             SDI Section 01
             Assignment: Methods
            */

            /*
             Problem #1: Painting a Wall
             Problem Description: Thils problem will prompt a user for 4 inputs. The first will be a number for the width of the wall in feet. The second will be also a
             number and this will be for the height of the wall. The next input will be another number for the amount of coats of paint that need to be applied. The last will be
             a final number for the surface area one gallon of paint can cover. The first step for us will be to welcome the user to our program and initilize some variables!
            */

            double wallWidth;
            double wallHeight;
            double numCoatsPaint; //We were between choosing an int here vs a double, but decided to go double just in case the user wants 2.5 coats on the wall, whatever reason that may be
            double surfaceAreaPaint;

            Console.WriteLine("Welcome to Steven Brown's Methods Program! In part 1 of our 3 part program, we will calculate how many gallons it will take to paint a wall! To figure it out, we will need some information.");
            //Now let's get that information! First we will ask and prompt the user for width of the wall
            Console.WriteLine("Please input the width of the wall in feet.");
            while (!double.TryParse(Console.ReadLine(), out wallWidth)) //We use a while loop that runs if the TryParse method fails to convert the user input into a double, meaning the input was not a valid number!
            {
                Console.WriteLine("You did not input a valid numeric value! Please input the width of the wall in feet!"); //Let them know what they did wrong!
            }

            Console.WriteLine("Next, please input the height of the wall in feet."); //Now we need to ask for the height of the wall!
            while (!double.TryParse(Console.ReadLine(), out wallHeight))  //Same deal as before, we are attempting to TryParse the user input, and if it fails, let the user know what they did wrong and reprompt
            {
                Console.WriteLine("You did not input a valid numeric value! Please input the wall height in feet!");
            }

            Console.WriteLine("Please input the number of coats of paint you will be applying"); //Same concepts applied as before, but this time for number of coats of paint
            while (!double.TryParse(Console.ReadLine(), out numCoatsPaint))
            {
                Console.WriteLine("You did not input a valid numeric value! Please input the number of coats of paint you will be applying!");
            }

            Console.WriteLine("Please input the surface area that one gallon of paint will cover (in feet squared)."); //Last but not least, again applying the same concepts, but we are requesting the surface area one gallon of paint will cover
            while (!double.TryParse(Console.ReadLine(), out surfaceAreaPaint))
            {
                Console.WriteLine("You did not input a valid numeric value! Please input the surface area that one gallon of paint will cover!");
            }

            /*
             At this point we have all our user input. We can now take this information and pass it to our custom method to determine the number of gallons needed. 
             NOTE: this method is located in the Problem #1 Methods section labeled below
            */

            double gallonsNeeded = DetermineGallonsNeeded(wallWidth, wallHeight, numCoatsPaint, surfaceAreaPaint); //Since the number of gallons could be a decimal, we use the double data type

            //Now that we have the answer to our question of to how many gallons of paint are needed, we can let the user know what we got!

            Console.WriteLine("For " + numCoatsPaint + " coat(s) on that wall, you will need " + gallonsNeeded.ToString().TrimStart('0') + " gallon(s) of paint."); //On a side note, the assignment shows that if the gallon is less than 1, we don't display the leading 0 (so 0.53 would be .53). To match this, we convert our number to a string to use the string methods, and then trim the 0 from the start!


            /* 
             Test Results
             Iteration 1
             wallWidth = 8, wallHeight = 10, numCoatsPaint = 2, surfaceAreaPaint = 300
             For 2 coat(s) on that wall, you will need .53 gallon(s) of paint.
             
             Iteration 2
             wallWidth = 30, wallHeight = 12.5, numCoatsPaint = 3, surfaceAreaPaint = 350
             For 3 coat(s) on that wall, you will need 3.21 gallon(s) of paint.

             Iteration 3
             wallWidth = 1, wallHeight = 1, numCoatsPaint = 5, surfaceAreaPaint = .5
             For 5 coat(s) on that wall, you will need 10 gallon(s) of paint.
             */

            /*
             Problem #2: Stung!
             Problem Description: For this problem, we will ask the user to input the weight of an animal (in lbs) and calculate how many bee stings are needed to kill the animal. 
             So let's welcome the user to part 2 and initilize some variables!
            */

            double animalWeight; //The animals weight could have a decimal so we will use double
            int lethalStings; //The number of stings it will take can't be a half a sting, so we will use int

            Console.WriteLine("\n\r\n\rWelcome to Part 2 of our program! In this part, we will calculate how many bee stings it would take to kill an animal! It takes 9 bee stings per pound! So we will need the animals weight!");

            //Now let's ask and prompt the user for the animals weight
            Console.WriteLine("Please input the weight of the animal in pounds.");
            while (!double.TryParse(Console.ReadLine(), out animalWeight)) //Like we did in the previous problem, we use the while loop to run if we are unable to parse the user input as a double, which would mean the user did not input a valid number!
            {
                Console.WriteLine("You did not enter a valid weight! Please input the animal's weight in pounds."); //Let them know where they went wrong before we reprompt them!
            }

            //We got the weight, so let's pass it to our custom method!
            lethalStings = DetermineLethalNumOfBeeStings(animalWeight);

            //Now we have the results, let's inform the user!
            Console.WriteLine("It takes " + lethalStings + " bee stings to kill this animal.");


            /* 
             Test Results
             Iteration 1
             animalWeight = 10
             It takes 90 bee stings to kill this animal.

             Iteration 2
             animalWeight = 160
             It takes 1440 bee stings to kill this animal.

             Iteration 3
             animalWeight = Twenty
             You did not enter a valid weight! Please input the animal's weight in pounds.

             Iteration 4
             animalWeight = .5
             It takes 5 bee stings to kill this animal.
             */

            /*
             Problem 3: Reverse it!
             Problem Description: For this problem, we will input a hard-coded array, use a custom method to reverse it's values 
             (without using the Array.Reverse() method, and return the new array. So to start, welcome the user and let's initilize some arrays!.

            */

            Console.WriteLine("\n\r\n\rWelcome to part 3 of our program! For this part we take an Array that is hard coded and reverse it! Let's see the results!");
            string[] initArray = new string[] { "apple", "pear", "peach", "coconut", "kiwi" }; //First we initilize the original Array
            //string[] initArray = new string[] { "red", "orange", "yellow", "green", "blue", "indigo", "violet" }; //Uncomment the front of the line for test purposes
            //string[] initArray = new string[] { "Bob", "Joe", "Fred", "Some other guy", "7", "cAt", "DoG" }; //Uncomment the front of the line for test purposes

            string[] reversedArray = new string[initArray.Length]; //Next we can initilize the revesedArray. We know the size should be equal to our orignial Array because we are just reversing the values, not adding or removing anything

            //We got our two Arrays! Lets run the original through our custom function and send the results to our reversed one!
            reversedArray = ReverseTheArray(initArray);

            //Now that all is said and done, let's output the orginial Array to the Console. We use String.Join to add a comma in between each Array value!
            Console.WriteLine("Your original Array was { \"" + string.Join("\", \"", initArray) + "\" } and now it is reversed as { \"" + string.Join("\", \"", reversedArray) + "\" }");

            /* 
             Test Results
             Iteration 1
             Your original Array was { "apple", "pear", "peach", "coconut", "kiwi" } and now it is reversed as { "kiwi", "coconut", "peach", "pear", "apple" }

             Iteration 2
             Your original Array was { "red", "orange", "yellow", "green", "blue", "indigo", "violet" } and now it is reversed as { "violet", "indigo", "blue", "green", "yellow", "orange", "red" }

             Iteration 3
             Your original Array was { "Bob", "Joe", "Fred", "Some other guy", "7", "cAt", "DoG" } and now it is reversed as { "DoG", "cAt", "7", "Some other guy", "Fred", "Joe", "Bob" }

            */
        }

        //--------------------------- Problem 1 Methods ------------------------------------------------//
        public static double DetermineGallonsNeeded(double width, double height, double coats, double surface) //This method returns a double and is passed 4 double values which we named accordingly
        {
            double gallons; //This is the total number of gallons that we will return at the end of the method
            double totalSurfaceArea = width * height; //First we need to figure out the total surface area of the wall, we do that by multiplying the width and the height (of the wall)
            gallons = totalSurfaceArea / surface; //Next we figure out how many gallons we would need for 1 coat, we do this by taking the total surface area we calculated in the previous line and divide it by the surface area a gallon of paint covers which was passed
            gallons *= coats; //Since we got the gallons of paint for 1 coat, we need to multiply that by the number of coats
            return Math.Round(gallons, 2); //Finally we can return the number of coats needed but first, lets round off the number to two decimal places
        }
        //--------------------------- Problem 2 Methods ------------------------------------------------//
        public static int DetermineLethalNumOfBeeStings(double weight) //This method returns an int and is passed one double which we named accordingly
        {
            /* 
             Since we passed the weight of the animal to our method, we can multiply that by 9 to get the number of stings it would take to kill the animal.
             However, the number could be a decimal value, and since that decimal means that the animal would require one full sting, we need to ensure we round up
             no matter what which we do using the Math.Ceiling method. Then finally we convert the double value to an integer before we return our results from the method!
            */
            int stingNum = Convert.ToInt32(Math.Ceiling((weight * 9))); 
            return stingNum;
        }
        //--------------------------- Problem 3 Methods ------------------------------------------------//
        public static string[] ReverseTheArray(string[] array) //This method returns a string Array and also is passed our original string Array which we named array
        {
            string[] reversedArray = new string[array.Length]; //First we need to initilized the reversed Array, and since we know that the length of the reversed Array is equal to the length of the orginial Array, we can use the array.length property to set the reversed Array's size
            int counter = 0; //Our counter will be working forwards as we move backwards!
            for(int i = array.Length - 1; i >= 0; i--) //Now here is where the magic happens! We use an int i as a second counter that will be moving backwards starting at the end of the Array. While i is greater than or equal to 0, run the Array! This ensures we can work through each index of the Array
            {
                reversedArray[counter] = array[i]; //So here we set the value of the reversedArray (Which will be moving forwards) to the value of our original Array (which is moving backwards)
                counter++; //Our i counter is already being deducted in the for loop, so we just need to add one the counter before the next loop iteration 
            }
            return reversedArray; //We have completed reversing the Array and can return that Array back from the function!
        }
    }
}
