﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brown_Steven_StringObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Steven Brown
             October 6, 2017
             SDI Section 1
             Assignment: String Objects
            */

            /* 
             Problem #1: Email Address Checker
             Problem Description: In this problem, a user will input a string which we will need to determine whether it is or not a valid email
             address. To make it a valid email, it can only have one @ symbol in the whole string, it can not have any spaces and have at least one dot after the 
             @ symbol. We take it a little more in depth but we can cover that when we explain method. So moving on, we need to welcome the user to our program, and prompt them for an email.
            */

            Console.WriteLine("Greetings! Welcome to Steven Brown's String Objects Program. The first part of this program requires you to input an email address and confirms it is of valid form. So let's get started!");

            Console.WriteLine("Please enter your email address!");
            string userEmail = Console.ReadLine(); //Here we save the user input as a string in a variable called userEmail

            /*
             Now we can send the user input into our custom made method called IsEmailReal. The assignment requires the method to return a string with either "valid" or "invalid".
             which we can then use to determine whether or not to reprompt the user since the email is invalid.
            */
            
            while (IsEmailReal(userEmail) != "valid") //While the user input is not valid, notify and reprompt the user NOTE: the IsEmailReal method can be found in the Problem 1 Method Section below
            {
                Console.WriteLine("The email address of " + userEmail + " is not a valid email address. Please enter a valid email address");
                userEmail = Console.ReadLine();
            }

            //Since we got to this point, we have determined that the user's inputted email address is valid and we can notifiy them as such.
            Console.WriteLine("The email address of " + userEmail + " is a valid email address.");



            /*
             Test Results 
             Iteration 1:
             userEmail = test@fullsail.com
             The email address of test@fullsail.com is a valid email address.

             Iteration 2:
             userEmail = test@full@sail.com
             The email address of test@full@sail.com is not a valid email address. Please enter a valid email address

             Iteration 3
             userEmail = test@full sail.com
             The email address of test@full sail.com is not a valid email address. Please enter a valid email address

             Iteration 4
             userEmail = test@.
             The email address of test@. is not a valid email address. Please enter a valid email address

            Iteration 5
            userEmail = !@fullsail.com
            The email address of !@fullsail.com is not a valid email address. Please enter a valid email address

            Iteration 6
            userEmail = test@fullsail.
            The email address of test@fullsail. is not a valid email address. Please enter a valid email address
            */

            /*
             Probelm #2: Separator Swap Out
             Problem Description: In this problem, the user inputs 3 strings. The first string is a list that utilizes some sort of separator. The second string inputted
             is the separator. Notice it specifies string, which means it can be more than one character! And the final inputted string is the separator that the user would
             like to replace the original separator with. Again, it is a string so it could be more than one character! So let's welcome the user to part 2 of our program and get started!
            */
            Console.WriteLine("\n\r\n\rWelcome to Part 2 of our program! Here we ask you for a list that is separated in some manner, whether it is by commas, dashes, slashes, numbers, etc." +
                " They only limit for a possible separator is your imagination! After you input the list, we will ask you for what you used for a separator, and then what you would like that separator" +
                " replaced with! So let's get started!");

            Console.WriteLine("Please input a list of items that are seperated in some maner (i.e. 1,2,3 or 1-2-3 or 1/2/3 etc. )");
            string list = Console.ReadLine(); //We store the user inputted list into a string variable named list

            Console.WriteLine("Please input the seperator used in the previously inputted string"); 
            string separator = Console.ReadLine(); //We store the orginial separator in a string variable named separator

            Console.WriteLine("Please input the new seperator you would like to replace your current seperator.");
            string newSeparator = Console.ReadLine(); //We store the new separator in a string variable named newSeparator

            //Now that we have all the information we need, we pass it to our custom method called ReplaceSeparator, which will return for us a new string. So we save the return value into a string called newList
            string newList = ReplaceSeparator(list, separator, newSeparator); //NOTE: This method can be found in the Problem #2 Method section below.

            //Now that all is said and done, we can output the original list, and then the new list with the new separators!
            Console.WriteLine("The original string of " + list + " with the new separator is " + newList + ".");

            /* 
             Test Results
             Iteration 1:
             list = 1,2,3,4,5 
             separator = , 
             newseparator = -
             The original string of 1,2,3,4,5 with the new separator is 1-2-3-4-5.
              
             Iteration 2:
             list = red: blue: green"
             separator = :
             newseparator = ,
             The original string of red: blue: green with the new separator is red, blue, green.

             Iteration 3:
             list = 1//2//3//4//5
             separator = //
             newseparator = |
             The original string of 1//2//3//4//5 with the new separator is 1|2|3|4|5.

             Iteration 4:
             list = -1---2--3--4
             separator = --
             newseparator = }-.-{
             The original string of -1---2--3--4 with the new separator is -1}-.-{-2}-.-{3}-.-{4.
            */
        }

        // ----------------------- Problem #1 Method ---------------------//
        public static string IsEmailReal(string email) 
        {
            /* 
             First we initialize a string that holds all the allowable characters for the email Address. Notice we have all letters, both uppercase and lowercase, numbers,
             and the @ symbol (and of course the dot symbol '.' No other symbols are allowed! 
            */
            string allowableCharacters = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890@."; //Notice a space is not in this string, so when we run the test later, we are also checking to make sure no spaces are inputted.

            /* 
             Now we start our conditional statements. Inside of our inner most conditional is a return statement that returns "valid". In order to get there, the inputted string
             needs to pass all of our tests. If it fails a single one, the email address is deemed not valid and it is returned as "invalid".
            */

            if(email.Contains('@')) //So to start out we first check to ensure an @ symbol exists
            {
                if(email.Count( x => x == '@') == 1) //Second we count the number of times the @ symbol appears in the string to ensure that there is only 1. This could have technically replaced our first conditional
                {
                    if (email.All(y => allowableCharacters.Contains(y))) //Next we use the All() method to run each character of the input string against our allowableCharacters string we created earlier to ensure the inputted string only has valid characters
                    {
                        //At this point we have confirmed that the user has used valid characters, and has only one @ symbol. So now we need to check to see if they have a dot after that @ symbol.
                        string[] splitEmail = email.Split('@');  //Here we break the inputted string up into two parts (I say two because we ensured there is only one @ symbol) and save the results in a string Array
                        if (splitEmail[1].Contains('.')) //Now we check to see if the string AFTER the @ symbol contains at least one dot
                        {
                            /*
                             Next we ensure that the user didn't input test@. or test@a. A valid email requires a domain (i.e. the fullsail part of test@fullsail.edu) and an extension (i.e. the .edu part of 
                             test@fullsail.edu) We can't test that the domain and extension are truly valid, but we can at least check to see if they are present in the email address, even if they are both
                             just one character (i.e. test@a.b would pass as valid since it has a domain present (the 'a' character) and an extension present (the 'b' character). So to sum it up, the string 
                             after the @ symbol should be atleast 3 characters, which we check in our next conditional statement.
                            */
                            if (splitEmail[1].Length >= 3) 
                            {
                                /*
                                 So great, the string after the @ symbol is atleast 3 characters and one of those is a dot, but we haven't ensured the user didn't input something like test@ab. or test@.ab. So 
                                 we check this next. We will do this by splitting up splitEmail[1] and cross checking the lengths of each value. We have to be careful here because the user could have entered multiple dots!
                                 So we can't assume there is just one! Also, a valid email doesn't have two dots next to each other so we will check that here!
                                 */
                                string[] splitEmailAfterAt = splitEmail[1].Split('.');
                                
                                foreach(string portion in splitEmailAfterAt) { //We use a foreach loop to cycle through each value of the splitEmailAfterAt Array
                                    if(portion.Length < 1) //The value should atleast be one character (which we already know is a valid chacter!)
                                    {
                                        return "invalid"; //Since there is not one character, immediately return "invalid". No need to waste resources by continuing to check the Array
                                    }
                                }

                                //Since the email made it to this point, it has passed all our tests and we can deem it the legendary and long sought after title of "valid"
                                return "valid";
                            }
                        }

                    }
                }

            }
            //Since the string did fail a test, it is awarded no points, and is deemed "invalid"
            return "invalid";
        }
        // ----------------------- Problem #1 Method ---------------------//
        public static string ReplaceSeparator(string list, string separator, string newSeparator)
        {
            /* 
             First we need to initialize some variables that we will be using for the remainder of method. The first one will be a string called newList
             which is where we will rebuild our list with the newSeparator. Next, we will create a string called testString which is where we will rebuild
             a string that is equal length of the separator string (it'll be explained in more detail as we go on!).
            */
            string newList = "";
            string testString = "";

            /*
             To start processing the list, we need to process each character individually, so we chose to use a for loop where we can manipulate the counter to effectively access
             the list's characters.
            */
            for(int x = 0; x < list.Length; x++) //So for this for loop, x is our counter will run while x is less than the length of the list. 
            {
                for(int y = 0; y < separator.Length; y++) //Now we need to build the testString which will be used to compare against the separator. The reason we did it this way was to account for separators of all size
                {
                    if (x + separator.Length < list.Length) //This check needs to be performed to ensure that we don't surpass the size of the Array! 
                    {
                        /* 
                         And here we is where we actually build the testString. So essentially if the string was a---b, and the separator was the 3 dashes (--- = 1 separator), we would start cycling through the characters
                         the first would be 'a'. But the separator is 3 characters so in order to check if there is a match, we also need 3 characters. So it starts at a, and then systematically moves down the characters in the string
                         until we have a string of equal size to compare to the separator string. The important thing is that we need to cycle through each character because a-- won't be a match the first iteration, but when the loop
                         goes through for the second iteration, '---' will equal '---' deeming it a match.
                       */
                        testString += list[x + y]; //The x denotes the character to start on, and y represents the distance from the starting character
                    }
                }
                if(testString == separator) //So now that we have the testString of equal length built, we can compare it to the separator
                {
                    newList += newSeparator; //Since we identified it as a match, we can add the newSeparator to our newList, effectively replacing the original separator
                    x += (separator.Length - 1); //Now we need to adjust which character to start on, because we just identified the separator, and if it the separator is more than one character, we don't won't to check the next character (which won't be a match, but still part of the separator) and add it to our newList
                } else //If the testString is not equal to the separator string, this character is not part of a separator and is safe to be inputted into our newList
                {
                    newList += list[x];
                }
                testString = ""; //We need to reset the testString to prepare it for the next round!
            }
            return newList; //We've finished building the newList, and now we are good to return it!
        }
    }
}
