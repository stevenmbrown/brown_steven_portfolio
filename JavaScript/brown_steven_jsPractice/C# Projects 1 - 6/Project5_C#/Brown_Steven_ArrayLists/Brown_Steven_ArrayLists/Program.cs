﻿using System;
using System.Collections; //This allows us to use ArrayLists in our program
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brown_Steven_ArrayLists
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Steven Brown
             October 8, 2017
             SDI Section 1
             ArrayLists Assignment
            */
            ArrayListAssignment(); // As per the assignment requirements, we call upon our custom made method here. It is a void method meaning it doesn't return a value
        }

        static void ArrayListAssignment()
        {
            /*
             For this assignment, we decided to use the name of several state's in one ArrayList and the state's corresponding capitals in the second ArrayList. It always helps to put things 
             in context so we will provide the user a brief explaination of what is going on!
            */
            Console.WriteLine("Welcome to Steven Brown's ArrayList Assignment. This program demonstrates our proficency working with ArrayLists. We chose to use an ArrayList with names of several states" +
                " and a second ArrayList with the states' corresponding capitals. There is no user input in this program so just sit back and enjoy the show!\n\r\n\r");

            //The first thing we need to do is initilize the ArrayLists. 

            ArrayList stateList = new ArrayList() { "Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa",
                "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska" };

            ArrayList capitalList = new ArrayList() { "Montgomery", "Juneau", "Phoenix", "Little Rock", "Sacramento", "Denver", "Harford", "Dover", "Tallahasse", "Atlanta", "Honolulu", "Boise", "Springfired", "Indianapolis",
                "Des Moines", "Topeka", "Franfort", "Baton Rouge", "Augusta", "Annapolis", "Boston", "Lansign", "St. Paul", "Jackson", "Jefferson City", "Helena", "Lincoln" };

            //Next the assignment calls for us to run a loop to output the data.
            Console.WriteLine("Below is a list of states with their respective capital cities!"); //Let the user know!
            foreach (string state in stateList) //We use a foreach loop to cycle through the entirety stateList
            {
                //Next we write a sentence that uses the values from both ArrayLists. We are able to correlerate the values in capitalList to the current state by using the index of the current state and using the same index value for the capital (since they should match)
                Console.WriteLine("The capital of {0} is {1}", state, capitalList[stateList.IndexOf(state)]);
            }

            //Next the assignment calls for us to remove two values from each ArrayList. We use two different methods to test their functionality
            stateList.RemoveAt(4); //This method removes the element at the index 4 (Which is California!)
            capitalList.RemoveAt(4); //We need to use the same index to ensure we remove the capital of California as well (Which is Sacramento!) 
            stateList.Remove("Kansas"); //This method removes the first occurance of "Kansas" in our ArrayList
            capitalList.Remove("Topeka"); //Again, we know the capital of Kansas is Topeka, so we can remove the first occurance of Topeka as well!

            //Now the assignment calls for us to add a value, so lets add North Carolina
            stateList.Insert ( 0, "North Carolina" ); //We chose to use the Insert method over the Add method becaue Insert allows us to chose which index value to add the element to, where as Add adds the element to the end
            capitalList.Insert ( 0, "Raleigh "); //We need to Insert our capital with the same index as well!

            //Finally we have to output our new updated ArrayLists to the console
            Console.WriteLine("\n\r\n\rNow, we took out two values from each ArrayList so two states should be missing! Can you find them? We also added one as well!\n\r"); 

            foreach (string state in stateList) //We use the same methodology as we did for the first foreach loop as we do here
            {
                Console.WriteLine("The capital of {0} is {1}", state, capitalList[stateList.IndexOf(state)]);
            }
        }
    }
}
