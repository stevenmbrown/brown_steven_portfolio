
function generateMenu(message, options) {
    let i = 1;
    let errorMessage = "";
    let input;
    let validOption = false;
    //Generate menu options and numbers
    message += `\n\n`;
    options.forEach(option => {
        message += `${i}) ${option}\n`;
        i++;
    });

    //User input prompt & validation
    do {
        input = prompt(errorMessage + message);
        if(input === null) return  options.length - 1;
        if (isNaN(parseInt(input))) { //Check first to see if the user entered a number or a string
            for (i = 1; i <= options.length; i++) { //see if the user entered string matches any of the options
                if (options[i-1].toLowerCase() === input.toLowerCase()) {
                    validOption = true;
                    input = i;
                }
            }
        } else {
            if (input > 0 && input <= options.length) { //is the integer one of the valid options
                validOption = true;
            }
        }
        if (!validOption) {
            errorMessage = "ERROR: You did not enter a valid option. Please try again.\n\n";
        }
    } while (!validOption) ;
    return input-1;
}

function validateFloat(message, min = "n", max = "n") {
    let userInput;
    let validFloat = false;
    do {
        userInput = prompt(message);
        if(userInput === null) return userInput;
        if(!isNaN(parseFloat(userInput))) {
            if(min !== "n") {
                if(max !== "n") {
                    if(userInput >= min && userInput <= max)
                        validFloat = true;
                } else {
                    if (userInput >= min)
                        validFloat = true;
                }
            } else if(max !== "n") {
                if(userInput <= max)
                    validFloat = true;
            } else { //Range isn't set
                validFloat = true;
            }
        }
        if(!validFloat && !message.includes("Error"))
            message += "\nError: You did not enter a valid number. Please try again.\n";
    } while(!validFloat);
    return parseFloat(userInput);
}


function tempConverter(from, to, temp) {
    switch(from) {

        case 0: //From Fahrenheit
            if(to === 0) //0 Because we spliced the Array making Celsius index 0
                return Math.round(((temp - 32) * (5 / 9)) * 100) / 100; //The *100 and /100 will help us round to 2 decimal places
            else
                return Math.round(((parseFloat(temp) + 459.67) * (5 / 9)) * 100) / 100;
        case 1: //From Celsius
            if(to === 0)//To Fahrenheit
                return Math.round(((temp * 1.8) + 32) * 100) / 100;
            else
                return Math.round((parseFloat(temp) + 273.15) * 100) / 100;
    }
    //From Kelvin
    if(to === 0) //To Fahrenheit else to Celsius
        return Math.round(((temp * 1.8 - 459.67))*100)/100;
    return Math.round((temp - 273.15) * 100) / 100;
}

function tempToString(temp) {
    switch (temp) {
        case 0:
            return "Fahrenheit";
        case 1:
            return "Celsius";
    }
    return "Kelvin";
}
function gasCheck(gas, gasPercent, mpg) {
    const remainingMiles = gas * (gasPercent / 100) * mpg;
    if(remainingMiles >= 200) //200 was set in the assignment specs
        return `Yes, you can drive ${remainingMiles} more miles and make it to the gas station`;
    return `You only have ${remainingMiles} miles you can drive. Better stop and and get gas while you can because you won't make it to the 200 miles to the next gas station!`;
}

function letterGrade(numericGrade) {
    if(numericGrade >= 90) //we know its not above 100 because of the ValidateFloatRange function
        return 'A';
    if(numericGrade >= 80) //We don't have to check if it is less than 90 because the function has already returned. Same concept for lower grades;
        return 'B';
    if(numericGrade >= 73)
        return 'C';
    if(numericGrade >= 70)
        return 'D';
    return 'F'; //If the function reaches it, it means the student has less than 70 meaning an F
}

function discountCheck(total) {
    if(total > 100)
        return ` qualified for a 10% discount which brings your grand total to ${(parseFloat(total) - (total * .1)).toLocaleString('en-US', { style: 'currency', currency: 'USD' })}.`;
    if(total > 50)
        return ` qualified for a 5% discount which brings your grand total to ${(parseFloat(total) - (total * .05)).toLocaleString('en-US', { style: 'currency', currency: 'USD' })}.`;
    return ` did not qualify for a discount meaning your grand total is ${parseFloat(total).toLocaleString('en-US', { style: 'currency', currency: 'USD' })}.`;
}

/////////////// Main Program \\\\\\\\\\\\\\
const menuOptions = ["Temperature Converter", "Last Chance for Gas", "Grade Letter Calculator", "Discount Double Check", "Exit" ];
let userChoice;
do {
    userChoice = generateMenu(`Welcome to my Conditionals Program
    
    I've created this menu for your convenience. Please select which scenario you would like to run by typing in the corresponding number or by typing out the name of the scenario.`, menuOptions);

    switch (userChoice) {
        case 0: //Temperature Converter
            let tempOptions = ["Fahrenheit", "Celsius", "Kelvin", "Cancel"];
            const fromMeasurement = generateMenu("Temperature Converter\n\nPlease select which unit you would like to convert from", tempOptions);
            if(fromMeasurement === 3) break;
            tempOptions.splice(fromMeasurement,1);
            const toMeasurement = generateMenu("Temperature Converter\n\nPlease select which unit you would like to convert to", tempOptions);
            if(toMeasurement === 2) break;
            const temperature = validateFloat("Temperature Converter\n\nPlease enter the temperature value you would like to convert: ");
            if(temperature === null) break;
            prompt(`Temperature Converter
            
            ${temperature} ${tempToString(fromMeasurement)} is equal to ${tempConverter(fromMeasurement, toMeasurement, temperature)} ${tempOptions[toMeasurement]}!
            
            Press any enter to return to the main menu!`);
            break;
        case 1: //Last Chance for Gas
            const currentGas = validateFloat("Last Chance for Gas\n\nThis scenario checks to see if you have enough gas to make it to the gas station that is in 200 miles. To start off, please enter how many gallons your gas tank can hold.", 0);
            if(currentGas === null) break;
            const gasPercentage = validateFloat("Last Chance for Gas\n\nPlease enter what percentage of gas you have left (0 - 100): ", 0, 100);
            if(gasPercentage === null) break;
            const mpg = validateFloat("Please enter how many miles per gallon your vehicle gets: ", 0);
            if(mpg === null) break;
            const gasCheckResults = gasCheck(currentGas, gasPercentage, mpg);
            prompt(`${gasCheckResults}
            
            Press enter to return to the main menu.`);
            break;
        case 2: //Letter Grade
            const numericGrade = validateFloat("Letter Grade\n\nFor this scenario, you need to input a number between 0 and 100 that corresponds to a student's average for the class. The program will then determine the appropriate letter grade for the student.\n\nPlease enter the numeric average of the student: ",0,100);
            if(numericGrade === null) break;
            prompt(`Letter Grade\n\nThe student's class average is ${numericGrade}%, which means the student has earned a(n) ${letterGrade(numericGrade)} in the class!\n\nPress enter to return to the main menu.`);
        break;
        case 3: //Discount Double Check
            const firstItem = validateFloat("Discount Double Check\n\nIn this scenario, the user is going to be purchasing 2 items from an online store. If the user spends $100 or more, they will receive a 10% discount. If the user spends between $50 - $99.99 dollars, they will receive a 5% discount. Anything less than that will result in no discount.\n\nPlease enter the price of the first item: ",0);
            if(firstItem === null) break;
            const secondItem = validateFloat("Discount Double Check\n\nPlease enter the price of the second item",0);
            if(secondItem === null) break;
            const purchaseTotal = parseFloat(firstItem) + parseFloat(secondItem);
            prompt(`Discount Double Check
            
            The subtotal of the two items together come out to: ${purchaseTotal.toLocaleString('en-US', { style: 'currency', currency: 'USD' })} which means you${discountCheck(purchaseTotal)}\n\nPress enter to return to the main menu.`);
    }
} while(userChoice !== menuOptions.length - 1);
prompt("Thank you for using our program. Press enter to exit.");
