//Steven Brown
//Count the Fish

function generateMenu(message, options) {
    let i = 1;
    let errorMessage = "";
    let input;
    let validOption = false;
    //Generate menu options and numbers
    message += `\n\n`;
    options.forEach(option => {
        message += `${i}) ${option}\n`;
        i++;
    });

    //User input prompt & validation
    do {
        input = prompt(errorMessage + message);
        if(input === null) return  options.length - 1;
        if (isNaN(parseInt(input))) { //Check first to see if the user entered a number or a string
            for (i = 1; i <= options.length; i++) { //see if the user entered string matches any of the options
                if (options[i-1].toLowerCase() === input.toLowerCase()) {
                    validOption = true;
                    input = i;
                }
            }
        } else {
            if (input > 0 && input <= options.length) { //is the integer one of the valid options
                validOption = true;
            }
        }
        if (!validOption) {
            errorMessage = "ERROR: You did not enter a valid option. Please try again.\n\n";
        }
    } while (!validOption) ;
    return input-1;
}

function getFishQuantity(fish, color) {
    let counter = 0;
    for(let i = 0; i < fish.length; i++) {
        if(color === fish[i]) {
            counter++;
        }
    }
    return counter;
}

//Main Program

const fish = ["Red", "Blue", "Green", "Yellow", "Blue", "Green", "Blue", "Blue", "Red", "Green"];

let menuOptions = [...new Set(fish)]; //Sets only hold unique values so by creating the menu options this way, we generate the options from the fish Array
menuOptions.push("Exit"); //Add an exit option

let userChoice;

do {
    userChoice = generateMenu(`Welcome to Steven Brown's Wonderful World of Fish Program! We've got fish for days! Want to know how many? Ask us by selecting an option!`, menuOptions);

    if(userChoice !== menuOptions.length -1)
        prompt(`There are ${getFishQuantity(fish, menuOptions[userChoice])} ${menuOptions[userChoice]} colored fish in the tank! Press the enter key to continue.`);

} while(userChoice !== menuOptions.length-1);

prompt("Thanks for using our program. Have a nice day! Press enter to exit the program.");




