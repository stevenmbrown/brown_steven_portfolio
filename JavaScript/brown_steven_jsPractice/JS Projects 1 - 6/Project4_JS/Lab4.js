
function generateMenu(message, options) {
    let i = 1;
    let errorMessage = "";
    let input;
    let validOption = false;
    //Generate menu options and numbers
    message += `\n\n`;
    options.forEach(option => {
        message += `${i}) ${option}\n`;
        i++;
    });

    //User input prompt & validation
    do {
        input = prompt(errorMessage + message);
        if(input === null) return  options.length - 1;
        if (isNaN(parseInt(input))) { //Check first to see if the user entered a number or a string
            for (i = 1; i <= options.length; i++) { //see if the user entered string matches any of the options
                if (options[i-1].toLowerCase() === input.toLowerCase()) {
                    validOption = true;
                    input = i;
                }
            }
        } else {
            if (input > 0 && input <= options.length) { //is the integer one of the valid options
                validOption = true;
            }
        }
        if (!validOption) {
            errorMessage = "ERROR: You did not enter a valid option. Please try again.\n\n";
        }
    } while (!validOption) ;
    return input-1;
}

function validateFloat(message, min = "n", max = "n") {
    let userInput;
    let validFloat = false;
    do {
        userInput = prompt(message);
        if(userInput === null) return userInput;
        if(!isNaN(parseFloat(userInput))) {
            if(min !== "n") {
                if(max !== "n") {
                    if(userInput >= min && userInput <= max)
                        validFloat = true;
                } else {
                    if (userInput >= min)
                        validFloat = true;
                }
            } else if(max !== "n") {
                if(userInput <= max)
                    validFloat = true;
            } else { //Range isn't set
                validFloat = true;
            }
        }
        if(!validFloat && !message.includes("Error"))
            message += "\nError: You did not enter a valid number. Please try again.\n";
    } while(!validFloat);
    return parseFloat(userInput);
}

function determineGallons(height, width, coats, surface) {

    return Math.round((((height * width) / surface) * coats) * 100) / 100;
}
function determineBeeStings(weight) {
    return Math.ceil(weight * 9);
}
function reverseArray(array) {
    let c = 0;
    let reversedArray = [];
    for(let i = array.length - 1; i >= 0; i--) {
        reversedArray[c] = array[i];
        c++;
    }
    return reversedArray;
}
const menuOptions = ["Painting a Wall", "Stung", "Reverse it!", "Exit"];
let userChoice;
do {
    userChoice = generateMenu("Welcome to our Methods Program. I created this menu for convenience\n\nPlease select one of the following scenarios by entering the numerical value associated with it or by typing in the option name", menuOptions)

    switch (userChoice) {
        case 0: //Painting a Wall
            const height = validateFloat("Painting a Wall\n\nIn this scenario, we will determine how much paint is needed to paint a wall.\n\nTo start, please enter the height of the wall in ft.", 0);
            if(height === null) break;
            const width = validateFloat("Painting a Wall\n\nNext, we will need the width of the wall in feet.", 0);
            if(width === null) break;
            const coats = validateFloat("Painting a Wall\n\nNow we need to know the number of coats of paint you will be applying (minimum 1 coat)", 1);
            if(coats === null) break;
            const surfaceArea = validateFloat("Painting a Wall\n\nFinally we need to know the surface area that one gallon of paint will cover (in feet squared). The minimum is one foot squared", 1);
            if(surfaceArea === null) break;
            prompt(`Painting a Wall
            
            Your ${height} x ${width} wall that requires ${coats} coats of paint will require ${determineGallons(height, width, coats, surfaceArea)} gallons of paint!
            
            Press enter to return to the main menu.`);
            break;
        case 1: //Stung!
            const weight = validateFloat("Stung!\n\nIn this scenario, we will determine the number of bee stings it would take to kill an animal. The rule of thumb is that it takes 9 bee stings per 1 pound to kill an animal.\n\nWhat is the weight of the animal?", 0);
            if(weight === null) break;
            prompt(`Stung!
            
            It would take ${determineBeeStings(weight)} bee stings to kill an animal that weighs ${weight} lbs!
            
            Press enter to return to the main menu.`);
            break;
        case 2: //Reverse it!
            const originalArray = ["apple", "pear", "peach", "coconut", "kiwi"];
            const reversedArray = reverseArray(originalArray);
            prompt(`Reverse it!
            
            The original array was:
            [${originalArray}]
            
            The array after being reversed is:
            [${reversedArray}]
            
            Cool? I know! Press any key to return to the main menu.`);
            break;

    }
} while(userChoice != menuOptions.length - 1);
prompt("Thanks for using our program.\n\nPress enter to exit.");