
let stateList =["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa",
    "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska" ];

let capitalList = [ "Montgomery", "Juneau", "Phoenix", "Little Rock", "Sacramento", "Denver", "Hartford", "Dover", "Tallahassee", "Atlanta", "Honolulu", "Boise", "Springfield", "Indianapolis",
    "Des Moines", "Topeka", "Frankfort", "Baton Rouge", "Augusta", "Annapolis", "Boston", "Lansing", "St. Paul", "Jackson", "Jefferson City", "Helena", "Lincoln"];

console.log(`Welcome to Steven Brown's JS version of the ArrayList Assignment. This program demonstrates our proficiency working with ArrayLists. 

Since ArrayLists do not exist in JavaScript, we will use normal Arrays because the main benefit of ArrayLists is allowing to change the size of the Array which Arrays already do in JS!

We chose to use our Array with names of several states and a second Array with the states' corresponding capitals. 

There is no user input in this program so just sit back and enjoy the show!`);

let loopResults = "";
for(let i = 0; i<stateList.length;i++) {
    loopResults += `The Capital of ${stateList[i]} is ${capitalList[i]}!\n`;
}

console.log(`STEP 1 - LOOP THROUGH THE STATES AND DISPLAY THE RESPECTIVE CAPITAL CITIES

${loopResults}`);

//Next we are required to remove two values from each Array
stateList.splice(4, 1);
capitalList.splice(4,1);
stateList.splice(stateList.indexOf("Kansas"),1);
capitalList.splice(capitalList.indexOf("Topeka"),1);

//Next we need to add a value
stateList.push("North Carolina");
capitalList.push("Raleigh");

loopResults = "";
for(let i = 0; i<stateList.length;i++) {
    loopResults += `The Capital of ${stateList[i]} is ${capitalList[i]}!\n`;
}

//Finally we need to output the new Arrays to the user
console.log(`STEP 2 - REMOVE TWO VALUES FROM EACH ARRAY AND ADD ONE NEW VALUE TO EACH ARRAY

${loopResults}`)

console.log(`That was all! We may come back and play with some more ES6 functionality but until then, Cheers!`);