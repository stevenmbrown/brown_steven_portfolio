//Steven Brown
//String Objects

function generateMenu(message, options) {
    let i = 1;
    let errorMessage = "";
    let input;
    let validOption = false;
    //Generate menu options and numbers
    message += `\n\n`;
    options.forEach(option => {
        message += `${i}) ${option}\n`;
        i++;
    });

    //User input prompt & validation
    do {
        input = prompt(errorMessage + message);
        if(input === null) return  options.length - 1;
        if (isNaN(parseInt(input))) { //Check first to see if the user entered a number or a string
            for (i = 1; i <= options.length; i++) { //see if the user entered string matches any of the options
                if (options[i-1].toLowerCase() === input.toLowerCase()) {
                    validOption = true;
                    input = i;
                }
            }
        } else {
            if (input > 0 && input <= options.length) { //is the integer one of the valid options
                validOption = true;
            }
        }
        if (!validOption) {
            errorMessage = "ERROR: You did not enter a valid option. Please try again.\n\n";
        }
    } while (!validOption) ;
    return input-1;
}

function validateString(message) {
    let userInput;
    do {
        userInput = prompt(message);
        if(!userInput && !message.includes("Error"))
            message += "\nError: You did not enter a valid string. Please try again.\n";

    } while(userInput === "");
    return userInput;
}

function validateEmail(email) {
    const allowableCharacters = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890@.";
    let atCounter = 0;

    //Test 1 - Ensure each character is of the allowable characters and count the @ symbols (since we are looping through anyways)
    for(let i = 0; i < email.length; i++) {
        if(!allowableCharacters.includes(email.charAt(i)))
            return false;
        if(email.charAt(i) === '@')
            atCounter++;
    }
    //Test 2 - Does the email have exactly 1 @ symbol?
    if(atCounter=== 1) {
        //Test 3 - TLDs cannot begin with a dot and double dots are not allowed
        if(!email.includes("@.") && !email.includes("..")) {
            //Test 4 - The email cannot be blank before the @ symbol and the tld needs a dot
            let parts = email.split('@');
            if(parts[0] && parts[1].includes('.')) {
                //Test 5 - Last step is to ensure at least the tld and domain are present
                parts = parts[1].split('.');
                if(parts[0] && parts[1])
                    return true;
            }
        }
    }
    return false;
}

function swapSeparator(input, oldS, newS) {
    const parts = input.split(oldS);
    let newString = "";
    if(!input.includes(oldS)) return `${input} ... because the separator '${oldS}' does not occur once in the list ${input}. `
    for(let i = 0; i < parts.length; i++) {
        if(i !== parts.length - 1)
            newString += parts[i] + newS;
        else
            newString += parts[i];
    }
    return newString;

}
let userChoice;
const menuOptions = ["Email Checker", "Separator Swapper", "Exit"];

do {
    userChoice = generateMenu(`Welcome to my String Object Program. This program is broken down into two scenarios and I created this menu for your convenience.
    
    The Email Checker prompts you to enter an email address and will notify you whether the email address is or is not valid.
    
    The Separator Swapper will take a list as input, then allow you to enter the separator you want changed, and then prompt you for the new separator you want to replace the old one with.
     
    Please select from the following options by typing in a number or the menu option name: `, menuOptions);

    switch (userChoice) {
        case 0:
            const email = prompt("Email Validator\n\nPlease enter an email");
            if(email === null) break;
            validateEmail(email) ? prompt(`The email: ${email} is valid. Press enter to return to main menu`) : prompt(`The email: ${email} is not valid. Please press enter to return to the main menu.`)
            break;
        case 1:
            const oldString = validateString("Separator Swap\n\nPlease enter a list separated by a character or sequence of characters");
            if(oldString === null) break;
            const oldSep = validateString("Separator Swap\n\nPlease enter the old separator.");
            if(oldSep === null) break;
            const newSep = validateString("Separator Swap\n\nPlease enter the new separator.");
            if(newSep === null) break;

            prompt(`Separator Swap\n\nWe swapped the separators around and we changed:\n${oldString}\nTo:\n${swapSeparator(oldString, oldSep,newSep)}\n\nPress enter to return to the main menu`);

    }

} while (userChoice !== menuOptions.length - 1);
prompt("Thanks for using our program! Press enter to exit");