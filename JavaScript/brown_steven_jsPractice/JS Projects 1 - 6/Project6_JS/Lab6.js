// Hero Class

class Hero {
    constructor(MaxHP) {
        this.maxHP = Number(MaxHP);
        this.minHP = 0;
        this.currentHP = MaxHP;
    }

    //Getters
    getMaxHP() {
        return this.maxHP;
    }

    getMinHP() {
        return this.minHP;
    }

    getCurrentHP() {
        return this.currentHP;
    }
    //Setters
    setMaxHP(newMaxHP) {
        this.maxHP = newMaxHP;
    }

    setCurrentHP(newCurrentHP) {
        this.currentHP = newCurrentHP;
    }

    changeCurrentHP(direction, amount) {
        let actualChange;
        if(direction === 1) { //HP is increasing
            if(this.currentHP + amount <= this.maxHP) { //Check to see if the change will exceed the max HP
                actualChange = amount;
                this.currentHP += amount;
            } else { //Else set the currentHP to maxHP
                actualChange = this.maxHP - this.currentHP;
                this.currentHP = this.maxHP;
            }
        } else { //The HP is decreasing
            if(this.currentHP - amount >= this.minHP) { //Will the hero survive?
                actualChange = amount;
                this.currentHP -= amount;
            } else {
                actualChange = this.minHP + this.currentHP;
                this.currentHP = this.minHP;
            }
        }
        return actualChange;
    }
}

//Functions


function generateMenu(message, options) {
    let i = 1;
    let errorMessage = "";
    let input;
    let validOption = false;
    //Generate menu options and numbers
    message += `\n\n`;
    options.forEach(option => {
        message += `${i}) ${option}\n`;
        i++;
    });

    //User input prompt & validation
    do {
        input = prompt(errorMessage + message);
        if(input === null) return  options.length - 1;
        if (isNaN(parseInt(input))) { //Check first to see if the user entered a number or a string
            for (i = 1; i <= options.length; i++) { //see if the user entered string matches any of the options
                if (options[i-1].toLowerCase() === input.toLowerCase()) {
                    validOption = true;
                    input = i;
                }
            }
        } else {
            if (input > 0 && input <= options.length) { //is the integer one of the valid options
                validOption = true;
            }
        }
        if (!validOption) {
            errorMessage = "ERROR: You did not enter a valid option. Please try again.\n\n";
        }
    } while (!validOption) ;
    return input-1;
}

function validateFloat(message, min = "n", max = "n") {
    let userInput;
    let validFloat = false;
    do {
        userInput = prompt(message);
        if(userInput === null) return userInput;
        if(!isNaN(parseFloat(userInput))) {
            if(min !== "n") {
                if(max !== "n") {
                    if(userInput >= min && userInput <= max)
                        validFloat = true;
                } else {
                    if (userInput >= min)
                        validFloat = true;
                }
            } else if(max !== "n") {
                if(userInput <= max)
                    validFloat = true;
            } else { //Range isn't set
                validFloat = true;
            }
        }
        if(!validFloat && !message.includes("Error"))
            message += "\nError: You did not enter a valid number. Please try again.\n";
    } while(!validFloat);
    return parseFloat(userInput);
}

//Main Program

const menuOptions = ["Increase Hero's HP", "Decrease Hero's HP", "Exit"];
let userChoice; let heroStatus; let amount;
const ourHero = new Hero(500);
do {
    if(ourHero.getCurrentHP() > ourHero.getMinHP())
        heroStatus = `Our hero is alive and well at ${ourHero.getCurrentHP()} HP.`;
    else
        heroStatus = `Our hero has fainted! We need you to increase life as soon as possible!`;
    userChoice = generateMenu(`Welcome to Steven Brown's Hero Program.
    
    ${heroStatus}
     
     Please make a selection from the following menu by entering the option number or entering the option name:`, menuOptions)

    switch (userChoice) {
        case 0: //increase HP
            amount = validateFloat("How much did our Hero's HP increase by?", 0);
            if(amount !== null) {
                const actualChange = ourHero.changeCurrentHP(1, amount);
                if(actualChange === amount)
                    prompt(`Our Hero's HP increased by ${amount}, bringing the current HP to ${ourHero.getCurrentHP()}.
                    
                    Press enter to return to the main menu.`);
                else {
                    if (actualChange === 0)
                        prompt(`Our Hero's HP did not change because our hero is already at max health! (${ourHero.getCurrentHP()} HP)
                    
                    Press enter to return to the main menu.`);
                    else
                        prompt(`Our Hero's HP increased by ${actualChange}, bring our Hero to full health (${ourHero.getCurrentHP()} HP)
                    
                    Press enter to return to the main menu.`);
                }
            }
            break;
        case 1: //Decrease HP
            amount = validateFloat("How much did our Hero's HP decrease by?", 0);
            if(amount !== null) {
                const actualChange = ourHero.changeCurrentHP(0, amount);
                if(actualChange === amount)
                    prompt(`Our Hero's HP decreased by ${amount}, bringing the current HP to ${ourHero.getCurrentHP()}.
                    
                    Press enter to return to the main menu.`);
                else {

                    if (actualChange === 0)
                        prompt(`Our Hero's HP did not change because our Hero's health is already at it's minimum! (${ourHero.getMinHP()} HP)
                    
                    Press enter to return to the main menu.`);
                    else
                        prompt(`Our Hero's HP decreased by ${actualChange}, bringing our Hero down to ${ourHero.getMinHP()} HP!
                    
                    Press enter to return to the main menu.`);
                }
            }
            break;
    }
} while(userChoice !== menuOptions.length - 1);
prompt("Thanks for using our program! Have a nice day!");