/* Character Class is the Parent Class the contains two children:
    Hero & Opponent.
 */
class Character {
    constructor(name) {
        this._name = name;
        this._inventory = [];
        this._armorEquip = { "helmet" : "None", "chest" : "None", "shield" : "None"};
        this._strength = getRandomInt(10);
        this._dexterity = getRandomInt(10);
        this._agility = getRandomInt(10);
        this._vitality = getRandomInt(10);
        this._currentHP = 100;
        this._gold = 0;
    }
    acquire(item) {
        this._inventory.push(item);
    }
    buy(item) {
        this.acquire(item);
        this._gold -= item.getSaleValue();
    }
    equip(armor) {
        const armorEquip = this.getArmorEquip();
        //First check to see if we are replacing armor or equipping it
        if(armorEquip[armor.getType()] !== "None") {
            const previousArmor = Armor.findArmor(armorEquip[armor.getType()]);
            this.acquire(previousArmor); //Put the item from being equipped back in the hero's inventory
            this.getArmorEquip()[armor.getType()] = armor.getName();
            log(`Replaced ${previousArmor.getName()} with ${armor.getName()}`, armor.getIcon());
        }
        else { //We are just equipping Armor
            this.getArmorEquip()[armor.getType()] = armor.getName();
            log(`Equipped ${armor.getName()}`, armor.getIcon());
        }
        this.removeItem(armor);
    }
    canAfford(item) {
        return this._gold - item.getSaleValue() > 0;
    }
    getName() {
        return this._name;
    }
    getHP() {
        return this._currentHP;
    }
    getCoreStats() {
        return {
            "Strength" : this._strength,
            "Dexterity" : this._dexterity,
            "Agility" : this._agility,
            "Vitality" : this._vitality
        };
    }
    getGold() {
        return this._gold;
    }
    getArmorEquip() {
        return this._armorEquip;
    }
    getDefenseTotal() { //Returns the total point value of armor equipped
        let total = 0;
        const armorEquip = this.getArmorEquip();
        if(armorEquip.helmet !== "None")
            total += Armor.findArmor(armorEquip.helmet).getDefense();
        if(armorEquip.chest !== "None")
            total += Armor.findArmor(armorEquip.chest).getDefense();
        if(armorEquip.shield !== "None")
            total += Armor.findArmor(armorEquip.shield).getDefense();
        return total;
    }
    getWeaponsInInventory() {
        return this._inventory.filter( (e) => e instanceof Weapon);
    }
    getArmorInInventory() {
        return this._inventory.filter( (i) => i instanceof Armor);
    }
    getInventory() {
        return this._inventory;
    }
    getInventoryForSale() {
        return this._inventory.map( (e) => {
            return `${e.getName()} ..... ${e.getValue()} gold`;
        });
    }
    increaseStrength(amt) {
        this._strength += amt;
    }
    increaseDexterity(amt) {
        this._dexterity += amt;
    }
    increaseAgility(amt) {
        this._agility += amt;
    }
    increaseVitality(amt) {
        this._vitality += amt;
    }
    isFaster(char) {
        return this._agility >= char._agility;
    }
    isInArmorRange(idx) {
        return idx >= 0 && idx < this.getArmorInInventory().length;
    }
    isInInventory(item) {
        return this._inventory.includes(Weapon.findWeapon(item)) || this._inventory.includes(Armor.findArmor(item));
    }
    isInInventoryRange(idx) {
        return idx >= 0 && idx < this._inventory.length;
    }
    isFainted() {
        return this._currentHP === 0;
    }
    move(item, char) {
        if (item instanceof Weapon) { //Is the player using a weapon
            var turn = item.attack(this._strength, char);
            char.takeDamage(turn.damage);
        } else if (item instanceof Armor) {
            var turn = hero.equip(item);
        }
        return turn;
    }
    receiveGold(amt) {
        this._gold += amt;
    }
    removeItem(item) {
        this._inventory.splice(this._inventory.indexOf(item), 1);
    }
    revive() {
        this._currentHP = this._vitality * 12;
    }
    selectItemInInventory(idx) {
        return this._inventory[idx];
    }
    sell(item) {
        this.removeItem(item);
        this.receiveGold(item.getValue());
    }
    selectItemArmorInInventory(idx) {
        const armorList = this.getArmorInInventory();
        return armorList[idx];
    }
    takeDamage(damage) {
        if(this._currentHP - damage > 0)
            this._currentHP -= damage;
        else
            this._currentHP = 0;
    }
}

class Hero extends Character {
    constructor(name) {
        super(name);
        this._strength = getRandomInt(12, 5); //Override so the user does a decent amount of damage
        this._vitality = getRandomInt(18, 5); //Override so the user has a decent amount of HP
        this._contractLvl = 1;
        this._contractNum = 0;
        this._inventory = [loadedData.Weapons[0], loadedData.Weapons[1]];
        this._currentContract;
        this._currentHP = this._vitality * 12;
        this._store;
    }
    contractCompletion() {
        this._contractNum += 1;
        const newContractLvl = Math.floor(this._contractNum / 3) + 1; //Contract Level increase every 3 contracts
        if(newContractLvl > this._contractLvl) {
            this._contractLvl++;
            return true; //Read next part of Narrative
        }
        return false;
    }
    getCurrentContract() {
        return this._currentContract;
    }
    getCurrentStore() {
        return this._store;
    }
    getCurrentOpponent() {
        return this._currentContract.getOpponent();
    }
    getCurrentContractLevel() {
        return this._contractLvl;
    }
    setCurrentContract(contract) {
        this._currentContract = contract;
    }
    updateStore() {
        this._store = new Store(this._contractLvl);
    }
}

class Opponent extends Character {
    constructor(name, strength, dexterity, agility, vitality, inventory, armorEquipped, gold) {
        super(name);
        this._strength = strength;
        this._dexterity = dexterity;
        this._agility = agility;
        this._vitality = vitality;
        this._currentHP = this._vitality * 12;
        this._inventory = inventory;
        this._armorEquip = armorEquipped;
        this._gold = gold;
    }
    getGold() {
        return this._gold;
    }
    selectItem() { //Randomly selects one item from the inventory
        return Weapon.findWeapon(this._inventory[Math.floor(Math.random() * Math.floor(this._inventory.length))]);
    }
    chanceToDrop() { //Determines if the opponent drops and item on defeat
        const item = this.selectItem();
        if(Math.floor(Math.random() * Math.floor(10)) + 1 >= 7)
            return item;
        return false;
    }
    static findOpponent(name) {//Find an opponent based on a string value
        return loadedData.Opponent.find( (e) => { return e._name.toLowerCase() === name.toLowerCase()} );
    }
}

/* The Item Class is the parent class for two sub classes:
Weapon & Armor.
 */
class Item {
    constructor(name, value, level, icon) {
        this._name = name;
        this._value = value;
        this._level = level;
        this._icon = icon;
    }
    getName() {
        return this._name;
    }
    getIcon() {
        return this._icon;
    }
    getValue() {
        return this._value;
    }
    getSaleValue() {
        return Math.ceil(this.getValue() + (this.getValue() * .50));
    }
}

class Weapon extends Item {
    constructor(name, value, level, type, maxDmg, minDmg, critChance, icon) {
        super(name, value, level, icon);
        this._type = type;
        this._maxDmg = maxDmg;
        this._minDmg = minDmg;
        this._critChance = critChance;
    }
    attack(strength, char) {
        const defenseTotal = char.getDefenseTotal(); //Get the Character being attack's defense value
        const strengthMultiplier = strength / 10 + 1; //Extra damage is dependent on Strength
        if(this.isCrit(this._critChance)) { //If it is a critical hit
            let damage = Math.floor(((Math.floor(Math.random() * Math.floor(this._maxDmg)) + this._minDmg)  * strengthMultiplier) - (defenseTotal/2) * 2);
            if(damage < 0) damage = 0; //If it did negative damage, set damage to 0
            return {
                "weapon" : this._name,
                "damage": damage,
                "crit": true
            };
        }
        //If it's not a critical hit, than just perform the normal damage algorithm
        let damage = Math.floor(((Math.floor(Math.random() * Math.floor(this._maxDmg)) + this._minDmg) * strengthMultiplier)) - defenseTotal;
        if(damage < 0) damage = 0;
        return {
            "weapon" : this._name,
            "damage": damage,
            "crit": false
        };
    }
    getType() {
        return this._type;
    }
    isCrit(chance) {
        return (Math.floor(Math.random() * Math.floor(100)) + 1) <= chance;
    }
    static findWeapon(name) { //Return a weapon object from a string
        return loadedData.Weapons.find( (e) => { return e._name.toLowerCase() === name.toLowerCase()} );
    }
}

class Armor extends Item {
    constructor(name, value, level, type, defense, icon) {
        super(name, value, level, icon);
        this._type = type;
        this._defense = defense;
    }
    getType() {
        return this._type;
    }
    getDefense() {
        return this._defense;
    }
    static findArmor(name) {
        return loadedData.Armor.find( (e) => { return e._name.toLowerCase() === name.toLowerCase()} );
    }
}

/* Contract is an object that holds details about each contract */
class Contract {
    constructor(id, name, description, value, opponent, difficulty, lvl) {
        this._id = id;
        this._name = name;
        this._description = description;
        this._value = value;
        this._opponent = opponent;
        this._difficulty = difficulty;
        this._lvl = lvl;
    }
    getID() {
        return this._id;
    }
    getName() {
        return this._name;
    }
    getDescription() {
        return this._description;
    }
    getDifficulty() {
        return this._difficulty;
    }
    getPayout() {
        return this._value;
    }
    getOpponent() {
        return this._opponent;
    }
    getContractLevel() {
        return this._lvl;
    }
}

/* Narrative Class is the Class that holds the storyline text */
class Narrative {
    constructor(title, part, text) {
        this._title = title;
        this._part = part;
        this._text = text;
    }
    getTitle() {
        return this._title;
    }
    logTranscript() {
        this._text.forEach((e) => {
            log(insertHeroName(e, hero.getName()), "story");
        });
    }
}

/* The Store Class holds information about the store. The Store changes every time the Hero
    completes a contract.
 */
class Store {
    constructor(level) {
        this._inventory = [...this.newInventory(level)];
    }
    addItemToInventory(item) {
        this._inventory.push(item);
    }
    getInventory() {
        return this._inventory;
    }
    getValidItems(level) {
        level++; //Gets potential items for the store based on the user level
        let validItems = [];
        loadedData.Armor.forEach( (i) => { if(i._level <= level) validItems.push(i) });
        loadedData.Weapons.forEach( (i) => { if(i._level <= level) validItems.push(i) });
        return validItems;
    }
    getSaleValue(i) {
        return Math.ceil(i.getValue() + (i.getValue() * .50));
    }
    getOptions() { //Generates an Array with details of the items in the store
        let options = [];
        this._inventory.forEach( (i) => options.push(`${i.getName()}.......${i.getSaleValue()} gold`) );
        return options;
    }
    isInStore(item) { //Checks if the item is in the Store
        return this._inventory.includes(Weapon.findWeapon(item)) || this._inventory.includes(Armor.findArmor(item));
    }
    isInStoreRange(idx) {
        return idx >= 0 && idx < this._inventory.length;
    }
    newInventory(level) {
        let inventory = new Set(); //We load the items as a set to make sure there are no duplicates
        let validItems = this.getValidItems(level);
        while(inventory.size < 6) { //Grab 6 items for the store's inventory
            inventory.add(validItems[Math.floor(Math.random() * Math.floor(validItems.length))]);
        }
        return inventory;
    }
    removeItemFromStore(item) {
        this._inventory.splice(this._inventory.indexOf(item), 1);
    }
    selectItemInStore(idx) {
        return this._inventory[idx];
    }
}