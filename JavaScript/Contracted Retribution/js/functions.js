/* Game Functionality */

/* Main flow control. input is the user input and i is parameter passed by the event listener which is used to
determine which part of the game the user is in and how to process that data. */
function playGame(input, i) {
    switch(i) {
        case 0: //Game just started, prompting user name
            startGame(input, i);
            break;
        case 1: //Introduction Narrative
            readNarrative(1, i);
            break;
        case 2: //User is sent to mercenary compound to pick a contract
            generateContractModal(i);
            break;
        case 100: //Start of a battle sequence
            preBattle();
            break;
        case 101: //User and opponent select and execute their moves until one faints
            battle(input);
            break;
        case 102: //Return to mercenary compound post battle
            returnToMercCompound();
            break;
        case 103: //User choice, enter store or sign new contract
            interpretUserAction(input);
            break;
        case 104: //Inside Shop
            visitStore(input);
            break;
        case 105: //Equipping Armor
            equipArmor(input);
            break;
        case 106: //User prompt upon leaving store/armor equip
            promptUserForAction();
            break;
        case 107: //Buy from store
            buyFromStore(input);
            break;
        case 108: //Sell items
            sellToStore(input);
            break;
        case 200: //Hero Was Slain
            heroWasSlain(input);
            break;
        case 201:
            log(`To start a new game, use the New Game Button.`);
            break;
        default:
    }
}
/* Function for defaulting variables and starting a new game*/
function startGame(input, i) {
    hero = new Hero(input);
    updateHeroStatisticsPane();
    updateUserOptions(null);
    updateInventoryPane("two");
    updateEventListeners(i);
    log(`Welcome ${hero.getName()}! To get started, press the Enter button!`);
}

/* Reads and processes the user input via the user input box */
function userInput(i) {
    //Grab the entered value
    const input = document.getElementById("user-text").value.trim();
    //Check to make sure it's a valid entry
    if(validateString(input) || i === 1 || i === 2 || i === 100 || i === 102 || i===106) { // 1, 2, 100 are exceptions (tells user to hit enter)
        playGame(input, i);
    }
    document.getElementById("user-text").value = ""; //Reset the text field after processing
}

/* Logs the message to the output console and to the browser console. */
function log(message, icon) {
    let e = document.getElementById("console");
    e.innerHTML += `<p><span class="${icon}">${message}</span></p>`;
    e.scrollTop = e.scrollHeight; //Make sure the scroll bar is all the way at the bottom
    console.log(message);
}

/* Contract Functions */

/* Returns 3 random contracts from the imported Contract Objects that are based on user level */
function generateContracts() {
    //If the hero has reached the final mission, just return that mission
    if(hero.getCurrentContractLevel() === 4) return [loadedData.Contract[loadedData.Contract.length-1]];
    // The contracts Array will hold the 3 contracts that are randomly selected
    let contracts = [];
    // First we will build an Array with valid Contracts
    let missionArray = loadedData.Contract.filter( (c) => c.getContractLevel() === hero.getCurrentContractLevel());
    let x;
    for(let i = 0; i < 3; i++) {
        //This do while loop will make sure that the randomly generated number is not one of the contracts already in the contracts Array
        do {
            x = getRandomInt(missionArray.length) - 1;
        } while(contracts.includes(missionArray[x]));
        //Add the contract to the contracts Array
        contracts.push(missionArray[x]);
    }
    return contracts;
}

/* Generates the Modal for the user to select one of three potential contracts */
function generateContractModal(i) {
    const contracts = generateContracts(); //Grab the three contracts
    const modal = document.getElementById('contractModal'); //Grab the HTML element
    let html = ""; //Build the HTML content for the modal
    contracts.forEach((contract) => {
        html += `<div class="contract-option">
                    <table>
                        <tr><th colspan="2">${contract.getName()}</th></tr>
                        <tr><th>Description</th><td>${contract.getDescription()}</td></tr>
                        <tr><th>Difficulty</th><td>${contract.getDifficulty()}</td></tr>
                        <tr><th>Payout</th><td>${contract.getPayout()} Gold</td></tr>
                        <tr><th>Opponent</th><td>${contract.getOpponent().getName()}</td></tr>
                        <tr><th>Sign Contract</th><td><button onclick="contractSign(${contract.getID()})">Sign Contract ${contract.getName()}</button></td></tr>
                    </table>
                 </div>`
    });
    document.getElementById('contract-content').innerHTML = html; //Update the element's HTML with the updated HTML
    modal.style.display = "block"; //Display the modal
    updateEventListeners(i); //Update the event listeners
}

/* This function runs when the users selects one of the contracts from the Modal to sign. Updates the Event Listeners and prepares for battle */
function contractSign(id) {
    closeModal(document.getElementById('contractModal')); //Close the Modal
    hero.setCurrentContract(loadedData.Contract[id]); //Set the current contract
    hero.getCurrentOpponent().revive(); //Make sure the opponent is at full health
    alert(`You accepted the ${loadedData.Contract[id].getName()} Contract.`);
    consoleBreak();
    log(`You accepted the ${loadedData.Contract[id].getName()} Contract.`);
    consoleBreak();
    log(`Mercenary Soldier: Very good. Good luck my friend`);
    log(`Press enter to launch the battle`);
    updateOpponentInfoPane(hero.getCurrentOpponent());
    updateUserOptions(null);
    updateEventListeners(99);
}

/* Battle Functions */

/* Continues to prepare the game for the impending battle. Displays contract information and updates event listeners */
function preBattle() {
    const contract = hero.getCurrentContract();
    if(hero.getWeaponsInInventory().length === 0) //Check if the Hero was a wise guy and sold all their items before taking a contract
        hero.acquire(Weapon.findWeapon("Punch"));
    updateInventoryPane();
    updateEventListeners(100);
    consoleBreak();
    log(`Contract: ${contract.getName()}`);
    log(`Opponent: ${contract.getOpponent().getName()}`);
    log(`Payout: ${contract.getPayout()}`);
    consoleBreak();
    log(`Please enter an item from your inventory, either the number or the item name`);

}

/* Handles user input and battle flow until someone faints */
function battle(input) {
    //Check if the user input was a valid item in the hero's inventory
    let item = validWeapon(input);
    //If it is not a valid weapon, check if it is a valid piece of Armor
    if(item === undefined) item = validArmor(input);
    if(item) {//If it is a valid Weapon or Armor, run the battle sequence and update the panes
        const opponent = hero.getCurrentOpponent();
        battleSequence(item, opponent);
        updateHeroStatisticsPane();
        updateOpponentInfoPane(opponent);
    } else { //The item was not valid
        log("You did not enter a valid item. Please enter either the number, or item name you wish to use. Press help for more information.");
    }
}

/* Runs through one iteration of the battle sequence */
function battleSequence(item, opponent) {
    let turn;
    if(hero.isFaster(opponent)) { //Determine who has more agility to determine turn order
        turn = hero.move(item, opponent); //Perform the hero's turn
        if(item instanceof Weapon) { //Is the hero using Weapon or Armor
            log(`${hero.getName()} used ${turn.weapon}. ${turn.crit ? `Critical Hit! ` : ``}${opponent.getName()} took ${turn.damage} damage.`);
        } else {
            updateInventoryPane();
            updateHeroStatisticsPane();
        }
        if(!opponent.isFainted()) { //If the opponent didn't faint, perform their turn and update the console
            turn = opponent.move(opponent.selectItem(), hero);
            log(`${opponent.getName()} used ${turn.weapon}. ${turn.crit ? `Critical Hit! ` : ``}${hero.getName()} took ${turn.damage} damage.`);
        }
    } else { //The opponent is faster than the hero so the opponent performs their move first
        turn = opponent.move(opponent.selectItem(), hero); //Perform the opponents move and update the console
        log(`${opponent.getName()} used ${turn.weapon}. ${turn.crit ? `Critical Hit! ` : ``}${hero.getName()} took ${turn.damage} damage.`);
        if(!hero.isFainted()) { //If the hero didn't faint, perform the hero's move and update the console
            turn = hero.move(item, opponent); //Perform the hero's move
            if(item instanceof Weapon) {  //Is the hero using a weapon, or Armor
                log(`${hero.getName()} used ${turn.weapon}. ${turn.crit ? `Critical Hit! ` : ``}${opponent.getName()} took ${turn.damage} damage.`);
            } else {
                updateInventoryPane();

                updateHeroStatisticsPane();
            }
        }
    }

    /* Handles in the event the Hero's HP reaches 0 */
    if(hero.isFainted()) {
        log(`${hero.getName()} has been slain...`);
        updateUserOptions(["Yes", "No"]);
        consoleBreak();
        log(`Would you like to try again?`);
        log(`1) Yes`);
        log(`2) No`);
        updateEventListeners(199);
    }
    if(opponent.isFainted()) {
        postBattle(hero.getCurrentContract());
        updateEventListeners(101);
    }
}

/* Handles post Battle results after a victorious battle */
function postBattle(contract) {
    //Grab the opponent so we don't have to constantly call the getOpponent method
    const opponent = contract.getOpponent();
    //Determine if the opponent dropped an item
    const dropItem = opponent.chanceToDrop();
    //These upgrades will be applied to hero's stats
    const upgrades = [getRandomInt(5,0), getRandomInt(5,0), getRandomInt(5,0), getRandomInt(5,0)];
    //Start logging to the console
    log(`${hero.getName()} was victorious!`);
    consoleBreak();
    if(dropItem) {
        log(`${opponent.getName()} dropped an item! ${hero.getName()} acquired ${dropItem.getName()}!`);
        hero.acquire(dropItem); //Add the dropped item to the hero's inventory
    }
    //Run through each of the upgrades and update the user as well as the hero's stats if it is above 0
    if(upgrades[0] !== 0) {
        log(`${hero.getName()}'s strength increased by ${upgrades[0]} points!`);
        hero.increaseStrength(upgrades[0]);
    }
    if(upgrades[1] !== 0) {
        log(`${hero.getName()}'s dexterity increased by ${upgrades[1]} points!`);
        hero.increaseDexterity(upgrades[1]);
    }
    if(upgrades[2] !== 0) {
        log(`${hero.getName()}'s agility increased by ${upgrades[2]} points!`);
        hero.increaseAgility(upgrades[2]);
    }
    if(upgrades[3] !== 0) {
        log(`${hero.getName()}'s vitality increased by ${upgrades[3]} points!`);
        hero.increaseVitality(upgrades[3]);
    }
    //Give the hero the gold the opponent was holding
    log(`${hero.getName()} found ${opponent.getGold()} gold!`);
    hero.receiveGold(opponent.getGold());
    consoleBreak();
    log(`Press enter to return to the Mercenary Compound`);
    updateUserOptions(null);
    updateHeroStatisticsPane();
}

/* Functions performed upon returning to the Mercenary Compound after contract completion */
function returnToMercCompound() {
    if(hero.getCurrentContract() === loadedData.Contract[loadedData.Contract.length-1]) //If the hero beat the final mission
        winGame();
    else {
        hero.receiveGold(hero.getCurrentContract().getPayout()); //Pay the hero the contract amout
        hero.revive(); //Max out the hero's HP
        if (hero.contractCompletion()) {
            alert(`Part ${hero.getCurrentContractLevel()}: ${loadedData.Narrative[hero.getCurrentContractLevel() - 1].getTitle()}`);
            readNarrative(hero.getCurrentContractLevel(), 101)
        } //Update the hero's contract completion number
        hero.updateStore(); //Update the store's inventory
        //Update the panes and event listeners
        updateInventoryPane("two");
        updateHeroStatisticsPane();
        updateEventListeners(102);
        //Log to the console
        consoleBreak();
        log(`** Upon returning back to the Mercenary Compound **`);
        log(`Mercenary Soldier: Well done ${hero.getName()}! As we agreed, here is your payout`);
        log(`** ${hero.getName()} receives ${hero.getCurrentContract().getPayout()} gold. **`);
        log(`Mercenary Soldier: Now that you have some gold, what would you like to do?`);
        promptUserForAction();
    }
}

/* Game Flow Functions */

/* Prompts the user whether they want to Visit the shop, sign a contract, or equip Armor */
function promptUserForAction() {
    consoleBreak();
    log(`What would you like to do?`);
    log(`Please select an option from the User Choice pane by entering the corresponding number or text.`)
    updateUserOptions(["Visit Shop", "Sign another Contract", "Equip Armor"]);
    updateEventListeners(102);
}

/* Handles the user's response from the PromptUserForAction method */
function interpretUserAction(input) {
    switch(input.toLowerCase()) {
        case "1":
        case "visit shop":
            enterStore();
            updateEventListeners(103);
            break;
        case "2":
        case "sign another contract":
            log(`** ${hero.getName()} enters Mercenary Compound. **`);
            log(`Mercenary Soldier: 'Aye! It's Captain's kid! What type of action are you searching for?`);
            log(`Press enter to select a contract`);
            updateEventListeners(1);
            break;
        case "3":
        case "equip armor":
            setupEquipArmor();
            updateEventListeners(104);
            break;
        default:
            log("You did not enter a valid item. Please enter either the number, or item name you wish to use. Press help for more information.");
    }
}

/* Runs when the Hero is slain in battle. Gives the user a choice to try again */
function heroWasSlain(input) {
    if(isNaN(input)) input = input.toLowerCase(); //If the user inputted a string, make it lowercase
    switch (input) {
        case "1":
        case "yes":
            /* User wants to try again so reset the battle field */
            hero.revive(); //Max Hero's health
            hero.getCurrentOpponent().revive(); //Max opponent's health
            // Update panes and update user on what happened
            updateUserOptions(null);
            updateHeroStatisticsPane();
            updateOpponentInfoPane(hero.getCurrentOpponent());
            log(`${hero.getName()} was revived!`);
            log(`Press enter to restart the battle!`);
            updateEventListeners(99);
            break;
        case "2": //End Game
        case "no":
            log(`Thanks for playing our game!`);
            log(`If you would like to play a new game, use the New Game button!`);
            updateEventListeners(200);
            clearPane("one");
            break;
        default:
            log(`You did not enter a valid option. Please enter 1 or Yes for Yes, or 2 or No for No`);
    }
}

/* Runs when the hero kills the final boss */
function winGame() {
    log(`** Cheers on the battlefield are heard as ${hero.getName()} connects with the final blow **`);
    log(`${hero.getName()}: It's over, it's finally over...`);
    log(`${hero.getName()}: Now for some well deserved rest.`);
    consoleBreak();
    log(`Congratulations! You beat the game! Press new game to play again!`);
    updateEventListeners(200);
}
/* Equip Armor Functions */

/* Prepares the interface for the user to select what Armor to equip */
function setupEquipArmor() {
    let options = hero.getArmorInInventory().map( (i) => i.getName());
    options.push("Exit Armor Equip");
    updateUserOptions(options);
    consoleBreak();
    log(`** ${hero.getName()} looks in bag **`);
    consoleBreak();
    log(`What should I equip?`);
    log(`Please select an item listed under User Options by entering the text or the number that corresponds with the item you would like to equip.`);
}

/* Equips Armor based on the user inputted string */
function equipArmor(input) {
    //Check to see if the user wants to exit
    if(input !== (hero.getArmorInInventory().length + 1).toString() && input.toLowerCase() !== "exit armor equip") {
        //Check if the input is a valid piece of Armor that is in the hero's inventory
        const item = validArmor(input);
        if (item) {
            //If it is valid, equip it
            hero.equip(item);
        } else {
            log("You did not enter a valid item. Please enter either the number, or item name you wish to use. Press help for more information.");
        }
        //Update the user option pane now that the Armor is equipped and not in the inventory
        let options = hero.getArmorInInventory().map((i) => i.getName());
        options.push("Exit Armor Equip");
        updateUserOptions(options);
        updateHeroStatisticsPane();
        updateInventoryPane("two");
    } else { //User wants to exit
        log(`Press enter to continue`);
        updateUserOptions(null);
        updateEventListeners(105);
    }
}

/* Store Functions */

/* Handles all setup for store */
function enterStore() {
    /* Generate user options based on the inventory of the current store */
    updateUserOptions(["Buy", "Sell", "Exit Store"]);
    consoleBreak();
    log(`** ${hero.getName()} enters the store **`);
    consoleBreak();
    log(`Shop Keeper: Welcome to our store! Please have a look around.`);
    log(`Please select whether you would like to buy or sell an item by selecting an action listed under User Options by entering the text or the number that corresponds with it.`);
}

/* Determines whether the user wants to buy, sell, or exit the store */
function visitStore(input) {
    if (isNaN(input)) input = input.toLowerCase(); //If the user entered a string, make it lowercase for switch statement
    let options; //Will be used later for user choices
    switch (input) {
        case "buy":
        case "1":
            log(`Shop Keeper: So you would like to buy some things, huh? Have a look.`);
            log(`Please select an item that you like to buy by typing it it's name or entering it's corresponding number.`);
            options = hero.getCurrentStore().getOptions();
            options.push("Return to Storefront");
            updateUserOptions(options);
            updateEventListeners(106);
            break;
        case "sell":
        case "2":
            log(`Shop Keeper: So you would like to sells some things, huh? Let's have a look.`);
            if(hero.getInventoryForSale().length !== 0) { //Make sure the users inventory is not empty
                log(`Please select an item that you like to sell by typing it it's name or entering it's corresponding number.`);
                options = hero.getInventoryForSale();
                options.push("Return to Storefront");
                updateUserOptions(options);
                updateEventListeners(107);
            } else {
                log(`Shop Keeper: Er, well... you need to have something to sell something!`);
            }
            break;
        case "exit store":
        case "3":
            log(`Shop Keeper: Have a good day kid!`);
            log(`** ${hero.getName()} exits the store **`);
            log(`Press enter to continue`);
            updateUserOptions(null);
            updateEventListeners(105);
            break;
        default:
            log("You did not enter a valid option. Please enter either the number, or the text of the action you would like to perform. Press help for more information.");
    }
}

/* Handles Buying an item from the store */
function buyFromStore(input) {
    const store = hero.getCurrentStore();
    if(input !== (store.getInventory().length + 1).toString() && input.toLowerCase() !== "return to storefront") { //Check to see if the user is exiting the store
        //Check if the item is valid and is in the store
        const validateItem = validStoreItem(input, store);
        if (validateItem) {
            //Check if the hero can afford the item
            if (hero.canAfford(validateItem)) {
                //If so, buy it
                hero.buy(validateItem);
                //Remove from store inventory
                store.removeItemFromStore(validateItem);
                log(`Shop Keeper: Pleasure doing business with you.`);
                log(`** ${hero.getName()} purchased ${validateItem.getName()} for ${validateItem.getSaleValue()} gold. **`);
                //Update user options now that the item is no longer in the store
                let options = hero.getCurrentStore().getOptions();
                options.push("Return to Storefront");
                updateUserOptions(options);
                updateInventoryPane("two");
                updateHeroStatisticsPane();
            } else {
                log(`Shop Keeper: Sorry kid, but it doesn't look like you can afford that item. Maybe try something else.`);
            }
        } else {
            log(`Invalid response. Please select an item listed under User Options by entering the text or the number that corresponds with the item you would like to buy.`);
        }
    } else { //return to storefront
        log(`Returning to Storefront.`);
        enterStore();
        updateEventListeners(103);
    }
}

/* Handles Selling an item to the store */
function sellToStore(input) {
    const store = hero.getCurrentStore();
    if(input !== (hero.getInventoryForSale().length + 1).toString() && input.toLowerCase() !== "return to storefront") { //Check to see if the user is exiting the store
        //Check if the item is valid and is in the hero's inventory
        const validateItem = validInventoryItem(input);
        if (validateItem) {
            hero.sell(validateItem);
            //Remove from Hero's inventory
            store.addItemToInventory(validateItem);
            log(`Shop Keeper: Pleasure doing business with you.`);
            log(`** ${hero.getName()} sold ${validateItem.getName()} for ${validateItem.getValue()} gold. **`);
            //Update user options now that the item is no longer in the inventory
            let options = hero.getInventoryForSale();
            //Check if it was the last item in the hero's inventory. If so, return to the storefront
            if(options.length === 0) {
                log(`That was the last item in your inventory. Returning to storefront`);
                updateUserOptions(["Buy", "Sell", "Exit Store"]);
                enterStore();
                updateEventListeners(103);
            } else //Otherwise, just add the option to return to storefront to the user choices
                options.push("Return to Storefront");
            updateUserOptions(options);
            updateInventoryPane("two");
            updateHeroStatisticsPane();
        } else {
            log(`Invalid response. Please select an item listed under User Options by entering the text or the number that corresponds with the item you would like to sell.`);
        }
    } else { //return to storefront
        log(`Returning to Storefront.`);
        enterStore();
        updateEventListeners(103);
    }
}

/* Validation Functions */

/* Determines if the item is one of the items in the Store's inventory and returns it */
function validStoreItem(input, store) {
    if(isNaN(input)) { //If the user did not input a number, try to find the item by name
        if (store.isInStore(input)) {
            //Check if the input is a weapon, if it is not, then it is Armor
            if (Weapon.findWeapon(input) === undefined)
                return Armor.findArmor(input);
            return Weapon.findWeapon(input);
        }
    } else { //Else check to see if that number is in range of the inventory indexes
        input--; //Minus 1 because we added one for usability
        if (store.isInStoreRange(input)) {
            return store.selectItemInStore(input);
        }
    }
    return false; //The item is not valid
}

/* Determines if the item is one of the items in the Hero's inventory and returns it */
function validInventoryItem(input) {
    if(isNaN(input)) { //If the user did not input a number, try to find the item by name
        if (hero.isInInventory(input)) {
            //Check if the input is a weapon, if it is not, then it is Armor
            if (Weapon.findWeapon(input) === undefined)
                return Armor.findArmor(input);
            return Weapon.findWeapon(input);
        }
    } else { //Else check to see if that number is in range of the inventory indexes
        input--; //Minus 1 because we added one for usability
        if (hero.isInInventoryRange(input)) {
            return hero.selectItemInInventory(input);
        }
    }
    return false; //The item is not valid
}

/* Determines if the user input is in fact a weapon in the hero's inventory */
function validWeapon(input) {
    if(isNaN(input)) { //If the user did not input a number, try to find the item by name
        if (hero.isInInventory(input)) return Weapon.findWeapon(input);
    } else { //Else check to see if that number is in range of the inventory indexes
        input--; //Minus 1 because we added one to the Array for readability
        if (hero.isInInventoryRange(input)) {
            return hero.selectItemInInventory(input);
        }
    }
    return false;
}

/* Determines if the user input is in fact a piece of Armor in the hero's inventory */
function validArmor(input) {
    if(isNaN(input)) { //If the user did not input a number, try to find the item by name
        if (hero.isInInventory(input)) return Armor.findArmor(input);
    } else { //Else check to see if that number is in range of the inventory indexes
        input--; //Minus 1 because we added 1 for readability
        if (hero.isInArmorRange(input)) {
            return hero.selectItemArmorInInventory(input);
        }
    }
    return false;
}

/* HTML Pane / Button Update Function */

/* Updates the Hero Statistics Pane with the latest info */
function updateHeroStatisticsPane() {
    const coreStats = hero.getCoreStats();
    const armorEquip = hero.getArmorEquip();
    let e = document.getElementById("hero-status");
    e.innerHTML = `
        <table>
        <tr><th>Name</th><td colspan="3">${hero.getName()}</td></tr>
        <tr><th>HP</th><td>${hero.getHP()} / ${coreStats.Vitality * 12}</td><th>Gold</th><td>${hero.getGold()}</td></tr>
        <tr><th>Strength</th><td>${coreStats.Strength}</td><th colspan="2">Armor</th></tr>
        <tr><th>Dexterity</th><td>${coreStats.Dexterity}</td><th>Helmet</th><td>${armorEquip["helmet"]}</td></tr>
        <tr><th>Agility</th><td>${coreStats.Agility}</td><th>Chest</th><td>${armorEquip["chest"]}</td></tr>
        <tr><th>Vitality</th><td>${coreStats.Vitality}</td><th>Shield</th><td>${armorEquip["shield"]}</td></tr>
        <tr></tr>
        </table>`;
}

/* Updates the user options pane with possible user inputs */
function updateUserOptions(options, pane = "one") {
    document.getElementById(`pane-${pane}-header`).innerHTML = "User Options"; //Change the header text
    let e = document.getElementById(`pane-${pane}`); //Grab the element
    //Setup the HTML to update the element
    let html = `<table>`;
    if(Array.isArray(options) && options.length > 0) {
        for(let i = 0; i < options.length; i++) {
            const j = i + 1;
            html += `<tr><td>${j}) ${options[i]}</td></tr>`;
        }
    } else {
        html += `<tr><td>1) Continue</td></tr>`;
    }
    html += `</table>`;
    //Finished setting up the HTML, so update the actual element
    e.innerHTML = html;
}

/* Updates the Battle Inventory Pane with an itemized list of items in the hero's inventory */
function updateInventoryPane(pane = "one") {
    document.getElementById(`pane-${pane}-header`).innerHTML = "Hero Inventory"; //Changes the header text
    let e = document.getElementById(`pane-${pane}`); //Grab the element
    const inventory = hero.getInventory();
    //Set up the HTML content
    let html = `<table>`;
    if(inventory.length === 0) { //If the inventory is empty, just add Empty to the table
        html += "<tr><td>Empty</td></tr>"
    } else {
        for(let i = 0; i < inventory.length; i++) {
            const idx = i + 1;
            html += `<tr><td>${idx}) ${inventory[i].getName()}</td></tr>`;
        }
    }
    html += "</table>";
    //Finished setting up the HTML, so update the actual element
    e.innerHTML = html;
}

/* Updates the Opponent Info Pane with current information about the opponent */
function updateOpponentInfoPane(o, pane="two") {
    document.getElementById(`pane-${pane}-header`).innerHTML = "Opponent Info"; //Update header text
    const coreStats = o.getCoreStats(); //grab the core stats (instead of making constant calls to the object)
    let e = document.getElementById(`pane-${pane}`); //Grab the element
    //Setup HTML updates
    let html = `<table>
        <tr><td colspan="2"><img src="images/icons/opponents/${o.getName().toLowerCase()}.gif" /> ${o.getName()}</td></tr>
        <tr><td>Current HP</td><td>${o.getHP()} / ${coreStats.Vitality * 12}</td></tr>
        <tr><td>Strength</td><td>${coreStats.Strength}</td></tr>
        <tr><td>Dexterity</td><td>${coreStats.Dexterity}</td></tr>
        <tr><td>Agility</td><td>${coreStats.Agility}</td></tr>
        <tr><td>Vitality</td><td>${coreStats.Vitality}</td></tr>
        </table>`;
    //Update the element with the updated HTML
    e.innerHTML = html;
}

/* Updates the Event Listeners to maintain the flow control of the program */
function updateEventListeners(i) {
    var x = parseInt(i) + 1;
    //Remove the all event listeners (since remove event listener isn't working the way we want it to)
    let old_element = document.getElementById("user-text");
    let new_element = old_element.cloneNode(true);
    old_element.parentNode.replaceChild(new_element, old_element);
    //Add the updated listener
    document.getElementById("user-text")
        .addEventListener("keyup", (e) => {
            e.preventDefault();
            if (e.keyCode === 13) {
                userInput(x);
            }
        });
    //Remove the all eventlisteners
    old_element = document.getElementById("user-submit");
    new_element = old_element.cloneNode(true);
    old_element.parentNode.replaceChild(new_element, old_element);
    //Add the updated listener
    document.getElementById("user-submit")
        .addEventListener("click", () => userInput(x));
    //Reset's the focus into the user input
    document.querySelector('input').focus();
}

/* HTML Page Button Functionality */

/* Clears the output console as well as the browser console */
function clearLog() {
    document.getElementById("console").innerHTML = ``; //Grab the Output Console element and clear it
    console.clear(); //Clear the browser console
}

/* Opens a Modal with help information */
function help() {
    const modal = document.getElementById('helpModal');
    modal.style.display = "block";
    window.onclick = (e) => {
        if (e.target == modal) {
            modal.style.display = "none";
        }
    }
}

/* Resets the game to a new state */
function newGame() {
    //Clear Panes
    clearLog();
    clearPane("one");
    clearPane("two");
    document.getElementById(`hero-status`).innerHTML = ``;
    hero = null;
    updateEventListeners(-1);
    log("Welcome to Contracted Retribution! Please enter your hero's name!");
}

/* Helper Functions */

/* Clears a Pane */
function clearPane(pane) {
    document.getElementById(`pane-${pane}`).innerHTML = ``;
}

/* Closes the Modal that is passed */
function closeModal(modal) {
    modal.style.display = "none";
}

/* Generates a long line in the console windows */
function consoleBreak() {
    log('----------------------------------------------------------');
}

/* Replaces the word $HERO$ in the narrative with the Hero's name */
function insertHeroName(string, name) {
    return string.replace("$HERO$", name);
}

/* Generates a random integer */
function getRandomInt(max, min = 1) {
    return Math.floor(Math.random() * Math.floor(max)) + min;
}

/* Function reads from the narrative and outputs it */
function readNarrative(part, i) {
    consoleBreak();
    part--;
    log(loadedData.Narrative[part].getTitle());
    consoleBreak();
    loadedData.Narrative[part].logTranscript();
    updateEventListeners(i);
}

/* Checks to see if a valid string was entered */
function validateString(input) {
    if(input === "")
        return false;
    return true;
}