/* Load Game Data */

var loadedData = {}; //Global Variable for Game Data
var hero; //Global Variable for the Hero Character

/* We're going to load data into a global variable. That variable stores all Weapons, Armor, Opponents, Contracts, and Narrative Objects
that are loaded in from the JSON data. Once this is complete, we will start the program using the .then() method
 */
const request = async () => {
    const response = await fetch('./js/data.json');
    const json = await response.json();
    loadedData.Weapons = json.Weapons.map((val) => {
        return new Weapon(val.name, val.value, val.level, val.type, val.maxDmg, val.minDmg, val.critChance);
    });
    loadedData.Armor = json.Armor.map((val) => {
        return new Armor(val.name, val.value, val.level, val.type, val.defense, val.icon);
    });
    loadedData.Opponent = json.Opponent.map((val) => {
        return new Opponent(val.name, val.strength, val.dexterity, val.agility, val.vitality, val.inventory, val.armorInventory, val.gold);
    });
    loadedData.Contract = json.Contract.map((val) => {
        return new Contract(val.id, val.name, val.description, val.value, Opponent.findOpponent(val.opponent), val.difficulty, val.lvl);
    });
    loadedData.Narrative = json.Narrative.map((val) => {
        return new Narrative(val.title, val.part, val.text);
    });
    //Add the event listeners
    /* User Input Event Listeners */
    document.getElementById("user-text")
        .addEventListener("keyup", (e) => {
            e.preventDefault();
            if (e.keyCode === 13) { //If the user pressed the enter key
                userInput(0);
            }
        });
    document.getElementById("user-submit").addEventListener("click", () => userInput(0));
    /* Modal Event Listeners */
    document.getElementsByClassName("close")[0].addEventListener("click", () => closeModal(document.getElementById("helpModal")));
};
request().then( () => {
    newGame(); //Now start the game
});