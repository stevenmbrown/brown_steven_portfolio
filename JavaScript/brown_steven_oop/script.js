
class Character { //Parent Class
    constructor(name) { //Constructor
        this._name = name; //Property
    }
    /* Encapsulation
        Encapsulation is used by making the name variable accessible via the getName method. Technically, there are no
        access modifiers in JavaScript so it is up to our own discipline to not call this._name outside the class
     */
    getName() { return this._name } //Method // Accessor

    /* Abstraction
        Abstraction is essentially the essential characteristics that distinguish an object. Here we are giving the Character the ability
        to both speak and move. Later, we give the monster class the ability to scare a character!
     */
    speak() { return `My name is ${this._name}!`; } // Method

    move(direction) { return `I am moved in 1 spot to the ${direction}`} //Method
}

/* Inheritance
    Inheritance is used with the Monster class inheriting the properties and methods of Character. We don't need to rewrite the
    getName method because the Monster Class inherits it!
 */
class Monster extends Character { //Child Class
    constructor(name, type) {
        super(name); //Call to parent class
        this._type = type; //Additional property

    }
    scare(character) { //Additional method
        return `Boo! ${this._name} scared ${character.getName()}`;
    }
    speak() {
        if(this._type === `beast`)
            return `Hoooowwllll`;
        return `Gergle Gergle ${name}! Arghhh!`;
    }
}

//Instantiation of Objects from both parent and child classes
const Bob = new Character("Bob");
const Tom = new Monster("Tom", "beast");
const Jim = new Monster("Jim", "gargoyle");
const charArray = [ Bob, Tom, Jim ];


console.log(Bob.speak());
console.log(Tom.speak());
console.log(`Hi, my name is ${Tom.getName()}`);

console.log(Jim.scare(Tom));
console.log(Bob.move("South"));
/* Polymorphism
    Polymorphism is being able to create a variable, function, or object that has more than one form. Here we are calling
    the speak() method that behaves differently dependant on object that calls it.
 */
charArray.forEach( (c) => console.log(c.speak()));
