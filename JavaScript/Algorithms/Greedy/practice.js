
/*
const canCompleteJobs = (startTimes, endTimes) => {
    const validity = startTimes.map( (startTime, i) => {
        if(i !== 0) {
            if (startTime < endTimes[i-1]) {
              return true;
            }
        }
        return false;
    });
    return !validity.includes(false);
};


const startTimes = [1,2,3];
const endTimes = [2,3,4];


console.log(canCompleteJobs(startTimes, endTimes));
*/

const n =50; //divisible by 5

const denominations = [20, 10, 5];

function payOut(n, denominations, i) {

    console.log(`Pay out ${Math.floor(n / denominations[i])} '${denominations[i]} bills'`);
    if(n%denominations[i] !== 0) {
        const newN = n%denominations[i];
        i++;
       payOut(newN, denominations, i);
    }
}

payOut(n, denominations, 0);
