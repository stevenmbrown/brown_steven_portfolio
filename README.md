# README #

### Repository Summary ###

* The purpose of this repository is to begin to build a portfolio of my work that I’ve developed throughout my Full Sail Web Design & Development Degree program so that I can provide proof of my developer skillset and industry knowledge to potential employers.   
* Version
* 1.0.0

### Repository Contents ###

* Coursework from WD2
* Additional self-initiated challenges

### Contact Information ###

* Repo owner: Steven Brown 
* Email: smbrown1@student.fullsail.edu
* Twitter: [@Design4TheWeb](https://twitter.com/Design4TheWeb)
* LinkedIn: https://www.linkedin.com/in/steven-b-07b0584a/
